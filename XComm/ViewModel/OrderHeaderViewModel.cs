﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class OrderHeaderViewModel
    {
        public long Id { get; set; }

        public string Reference { get; set; }

        public decimal Amount { get; set; }

        public List<OrderDetailViewModel> details { get; set; }

    }

    public class OrderDetailViewModel
    {
        public OrderDetailViewModel()
        {
            Quantity = 1;
        }
        public long Id { set; get; }

        public long HeaderId { get; set; }
        public long ProductId { get; set; }

        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
    }

    public class CartViewModel: ProductViewModel
    {

        public decimal Quantity { get; set; }
    }
}
