﻿using System.Security.Cryptography;
using System.Text;

namespace Framework.Auth
{
    public class Encryption
    {
        public static string Hashsha256(string rawData)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                //compute hash
                byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                //convert byte to array
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
        //yang lebih aman kyaknya gini
        public static byte[] GenerateSalt()
        {
            byte[] salt = new byte[32];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(salt);
            }
            return salt;
        }

        public static string HashPassword(string password, byte[] salt)
        {
            using (var sha256 = new SHA256Managed())
        {
            // Concatenate password and salt
            byte[] combined = Encoding.UTF8.GetBytes(password + Convert.ToBase64String(salt));

            // Compute the hash
            byte[] hashBytes = sha256.ComputeHash(combined);

            // Return the Base64-encoded hash
            return Convert.ToBase64String(hashBytes);
        }
        }
    }
}