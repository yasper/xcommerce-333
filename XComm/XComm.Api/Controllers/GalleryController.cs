﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Repositories;
using XComm.Api.Security;

namespace XComm.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GalleryController : ControllerBase
    {

        private GalleryRepository _repo;

        public GalleryController(XCommDbContext dbContext)
        {
            _repo = new GalleryRepository(dbContext);
        }

        [HttpGet]
        public async Task<ResponseResult> Get(int pageNum, int rows, string? search = "", string? orderBy = "", Sorting sort = Sorting.Ascending)
        {
            return _repo.Pagination(pageNum, rows, search, orderBy, sort);
        }

        [HttpPost]
        [ReadableBodyStream(Roles = "Administrator, Galleries")]
        public async Task<GalleryViewModel> Post(GalleryViewModel model)
        {
            return _repo.Create(model);
        }
        [HttpGet("getAll")]
        public async Task<List<GalleryViewModel>> GetVariant()
        {
            return _repo.GetAll();
        }

        [HttpPut("{id}")]
        [ReadableBodyStream(Roles = "Administrator, Galleries")]
        public async Task<GalleryViewModel> Put(long id, GalleryViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }
        [HttpGet("{id}")]
        public async Task<GalleryViewModel> GetById(int id)
        {
            return _repo.GetById(id);
        }
    }
}
