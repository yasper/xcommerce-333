﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Repositories;
using XComm.Api.Security;

namespace XComm.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private OrderRepositories order;

        public OrderController(XCommDbContext dbContext)
        {
            order = new OrderRepositories(dbContext);
        }

        [HttpPost]
        public async Task<OrderHeaderViewModel> Post(OrderHeaderViewModel model)
        {
            return order.Create(model);
        }

    }
}
