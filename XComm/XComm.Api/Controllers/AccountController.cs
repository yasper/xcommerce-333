﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Repositories;

namespace XComm.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {


        private AccountRepositories _repo;
        private readonly IConfiguration _configuration;

        public AccountController(XCommDbContext dbContext, IConfiguration configuration)
        {
            _configuration = configuration;
            _repo = new AccountRepositories(dbContext);
        }
        [HttpPost]
        public async Task<AccountViewModel> Post(LoginViewModel model)
        {
            AccountViewModel result = new AccountViewModel();
            if (ModelState.IsValid)
            {
                result = _repo.Authentication(model);
                if (result != null)
                {
                    var claims = new List<Claim>()
                    {
                        new Claim(ClaimTypes.Name, result.UserName),
                        new Claim("FirstName", result.FirstName),
                        new Claim("LastName", result.LastName),
                    };

                    foreach (var item in result.Roles)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, item));
                    }


                    var token = GetToken(claims);
                    result.Token = new JwtSecurityTokenHandler().WriteToken(token);
                }
                else
                {
                    return null;
                }
            }
            return result;
        }

        private JwtSecurityToken GetToken(List<Claim> claims)
        {
            var authSigninKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
            var token = new JwtSecurityToken(
                issuer: _configuration["JWT:Issuer"],
                audience: _configuration["JWT:Audience"],
                claims: claims,
                expires: DateTime.Now.AddDays(Convert.ToDouble(_configuration["JWT:Expires"])),
                signingCredentials: new SigningCredentials(authSigninKey, SecurityAlgorithms.HmacSha256)
                );
            return token;
        }
        [HttpPost("register")]
        public async Task<AccountViewModel> Post(RegisterViewModel model)
        {
            return _repo.Authentication(model);
        }

        [HttpGet]
        public async Task<List<AccountViewModel>> GetAccount()
        {
            return _repo.GetAccounts();
        }
    }
}
