﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Repositories;
using XComm.Api.Security;

namespace XComm.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {

        private CategoryRepository _repo;

        public CategoryController(XCommDbContext dbContext)
        {
            _repo = new CategoryRepository(dbContext);
        }

        [HttpPost]
        [ReadableBodyStream(Roles = "Administrator, Categories")]
        public async Task<CategoryViewModel> Post(CategoryViewModel model)
        {
            return _repo.Create(model);
        }

        [HttpGet]
        public async Task<ResponseResult> Get(int pageNum, int rows, string? search = "", string? orderBy = "", Sorting sort = Sorting.Ascending)
        {
            return _repo.Pagination(pageNum, rows, search, orderBy, sort);
        }


        [HttpGet("getAll")]
        public async Task<List<CategoryViewModel>> Get()
        {
            return _repo.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<CategoryViewModel> GetById(int id)
        {
            return _repo.GetById(id);
        }

        [HttpPut("{id}")]
        [ReadableBodyStream(Roles = "Administrator, Categories")]
        public async Task<CategoryViewModel> Put(long id, CategoryViewModel? model)
        {
            model.Id = id;
            return _repo.Update(model);
        }

        [HttpPut("changestatus/{id}")]
        [ReadableBodyStream(Roles = "Administrator, Categories")]
        public async Task<CategoryViewModel> Put(long id, bool status)
        {
            return _repo.ChangeStatus(id, status);
        }
    }
}
