﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Repositories;
using XComm.Api.Security;

namespace XComm.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private CartRepositories _cart;

        public CartController(XCommDbContext dbContext)
        {
            _cart = new CartRepositories(dbContext);
        }

        
        [HttpGet]
        [ReadableBodyStream]
        public async Task<ResponseResult> Post(int pageNum, int rows, string? search = "", string? orderBy = "", Sorting sort = Sorting.Ascending)
        {
            return _cart.Pagination(pageNum, rows, search, orderBy, sort);
        }
    }
}
