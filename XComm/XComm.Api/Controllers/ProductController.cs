﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Repositories;
using XComm.Api.Security;

namespace XComm.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private ProductRepository _repoProduct;
        public ProductController(XCommDbContext dbContext)
        {
            _repoProduct = new ProductRepository(dbContext);
        }

        [HttpPost]
        [ReadableBodyStream(Roles = "Administrator, Products")]
        public async Task<ProductViewModel> Post(ProductViewModel model)
        {
            return _repoProduct.Create(model);
        }

        [HttpGet]
        public async Task<ResponseResult> GetProduct(int pageNum, int rows, string? search = "", string? orderBy = "", Sorting sort = Sorting.Ascending)
        {
            return _repoProduct.Pagination(pageNum, rows, search, orderBy, sort);
        }
        [HttpGet("{id}")]
        public async Task<ProductViewModel> GetById(int id)
        {
            return _repoProduct.GetById(id);
        }


        [HttpPut("{id}")]
        [ReadableBodyStream(Roles = "Administrator, Products")]
        public async Task<ProductViewModel> Put(long id, ProductViewModel? model)
        {
            model.Id = id;
            return _repoProduct.Update(model);
        }

        [HttpPut("changestatus/{id}")]
        [ReadableBodyStream(Roles = "Administrator, Products")]
        public async Task<ProductViewModel> Put(long id, bool status)
        {
            return _repoProduct.ChangeStatus(id, status);
        }
        [HttpPut("changeimage/{id}/{galleryId}")]
        [ReadableBodyStream(Roles = "Administrator, Products")]
        public async Task<ProductViewModel> Put(long id, long galleryId)
        {
            return _repoProduct.ChangeImage(id, galleryId);
        }
    }
}
