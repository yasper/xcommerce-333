﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using XComm.Api.Models;

namespace XComm.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        public static List<Student> StudentList = new List<Student>()
        {
            new Student(){ Id=1, FirstName="Auriwan", LastName="Yasper", Age=20},
            new Student(){ Id=2, FirstName="Okin", LastName="Niko", Age=20},
            new Student(){ Id=3, FirstName="Muhammad", LastName="Budi", Age=20},
        };

        [HttpGet]
        public async Task<List<Student>> Get()
        {
            return StudentList;
        }

        [HttpGet("id")]
        public async Task<Student> Get(int id)
        {
            Student student = StudentList.Where(o => o.Id == id).First();
            return student != null ? student : new Student();
        }
    }
}
