﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Repositories;
using XComm.Api.Security;

namespace XComm.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VariantController : ControllerBase
    {
        private VariantRepository _repoVariant;
        public VariantController(XCommDbContext dbContext)
        {
            _repoVariant = new VariantRepository(dbContext);
        }

        [HttpPost]
        [ReadableBodyStream(Roles = "Administrator, Variants")]
        public async Task<VariantViewModel> Post(VariantViewModel model)
        {
            return _repoVariant.Create(model);
        }

        [HttpGet]
        public async Task<ResponseResult> GetVariant(int pageNum, int rows, string? search = "", string? orderBy = "", Sorting sort = Sorting.Ascending)
        {
            return _repoVariant.Pagination(pageNum, rows, search, orderBy, sort);
        }


        [HttpGet("getAll")]
        public async Task<List<VariantViewModel>> GetVariant()
        {
            return _repoVariant.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<VariantViewModel> GetById(int id)
        {
            return _repoVariant.GetById(id);
        }


        [HttpPut("{id}")]
        [ReadableBodyStream(Roles = "Administrator, Variants")]
        public async Task<VariantViewModel> Put(long id, VariantViewModel? model)
        {
            model.Id = id;
            return _repoVariant.Update(model);
        }

        [HttpPut("changestatus/{id}")]
        [ReadableBodyStream(Roles = "Administrator, Variants")]
        public async Task<VariantViewModel> Put(long id, bool status)
        {
            return _repoVariant.ChangeStatus(id, status);
        }

        [HttpGet("category/{id}")]
        public async Task<List<VariantViewModel>> GetByParent(long id)
        {
            return _repoVariant.GetByParentId(id);
        }
    }
}
