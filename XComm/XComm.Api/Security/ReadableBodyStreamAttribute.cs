﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;

namespace XComm.Api.Security
{
    public class ReadableBodyStreamAttribute : AuthorizeAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var userName = context.HttpContext.User.Claims.First().Value;
            new ClaimContext(userName);
        }
    }

    public class ClaimContext
    {
        private static string _UserName;

        public ClaimContext(string userName)
        {
            _UserName = userName;
        }
        public static string UserName()
        {
            return _UserName;
        }
    }
}
