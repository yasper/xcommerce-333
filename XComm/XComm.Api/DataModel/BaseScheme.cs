﻿using System.ComponentModel.DataAnnotations;

namespace XComm.Api.DataModel
{
    //dipakai di banyak entity
    public class BaseScheme
    {
        [Required, MaxLength(50)]//annotation
        public string CreatedBy { get; set; }
        public DateTime CreatedDate{ get; set; }

        [MaxLength(50)]
        public string? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public bool Active { get; set; }
    }
}
