﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace XComm.Api.DataModel
{
    [Table("TransOrderHeader")]
    public class OrderHeader: BaseScheme
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, MaxLength(15)]
        public string Reference {  get; set; }
        
        public decimal Amount { get; set; }
    }

    [Table("TransOrderDetails")]
    public class OrderDetail: BaseScheme
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id {  set; get; }

        public long HeaderId {  get; set; }
        public long ProductId { get; set; }

        public decimal Quantity { get; set; }
        public decimal Price { get; set; }


        [ForeignKey("HeaderId")]
        public virtual OrderHeader Header { get; set; }

        [ForeignKey("ProductId")]
        public virtual Products Products { get; set; }
    }

    [Table("Carts")]
    public class Cart : BaseScheme
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { set; get; }

        public long ProductId { get; set; }

        public decimal Quantity { get; set; }

        [ForeignKey("ProductId")]
        public virtual Products Products { get; set; }
    }
}
