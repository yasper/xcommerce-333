﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace XComm.Api.DataModel
{
    [Table("MasterUserRoles")]
    public class UserRoles: BaseScheme
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required, MaxLength(50)]
        public string UserName {  get; set; }

        [Required, MaxLength(100)]
        public string Role { get; set; }


        [ForeignKey("UserName")]
        public virtual Accounts Username { get; set; }
    }
}
