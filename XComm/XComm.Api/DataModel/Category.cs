﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XComm.Api.DataModel
{
    //entity kategory     //inheritence
    [Table("MasterCategories")]
    public class Category: BaseScheme
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [Required,MaxLength(10)]
        public string Initial { get; set; }

        [Required,MaxLength(50)]
        public string Name { get; set; }

    }
}
