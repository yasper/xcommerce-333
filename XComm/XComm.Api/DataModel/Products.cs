﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XComm.Api.DataModel
{
    [Table("MasterProducts")]
    public class Products: BaseScheme
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public long VariantId { get; set; }

        public long? GalleryId { get; set; }
        
        [Required, MaxLength(10)]
        public string Initial { get; set; }

        [Required, MaxLength(50)]
        public string Name { get; set; }

        [Required, MaxLength(500)]
        public string Description { get; set; }



	    public decimal Price {  get; set; }
	    public decimal Stock { get; set; }

        [ForeignKey("VariantId")]
        public virtual Variants Variants { get; set; }


        [ForeignKey("GalleryId")]
        public virtual Gallery Gallery { get; set; }
    }
}
