﻿using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Security;

namespace XComm.Api.Repositories
{
    public class CategoryRepository : IRepository<CategoryViewModel>
    {
        private XCommDbContext _dbContext;
        private ResponseResult _result = new ResponseResult();
        public CategoryRepository(XCommDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public CategoryViewModel ChangeStatus(long id, bool status)
        {
            CategoryViewModel result = new CategoryViewModel();
            try
            {
                Category entity = _dbContext.Categories
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Active = status;

                    entity.ModifiedBy = ClaimContext.UserName();
                    entity.ModifiedDate = DateTime.Now;

                    _dbContext.SaveChanges();

                    result = new CategoryViewModel()
                    {
                        Id = entity.Id,
                        Active = status,
                        Initial = entity.Initial,
                        Name = entity.Name
                    };
                }
                // else
                // {
                // _result.Success = false;
                // _result.Message = "Category Not Found";
                // }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public CategoryViewModel Create(CategoryViewModel model)
        {
            try
            {
                Category entity = new Category();
                entity.Name = model.Name;
                entity.Initial = model.Initial;
                entity.Active = model.Active;
                entity.CreatedBy = ClaimContext.UserName();
                entity.CreatedDate = DateTime.Now;
                _dbContext.Categories.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;
            }
            catch (Exception)
            {
                throw;
            }

            return model;
        }

        public List<CategoryViewModel> GetAll()
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            try
            {
                result = (from o in _dbContext.Categories
                          select new CategoryViewModel
                          {
                              Id = o.Id,
                              Name = o.Name,
                              Active = o.Active,
                              Initial = o.Initial
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public CategoryViewModel GetById(long id)
        {
            CategoryViewModel model = new CategoryViewModel();
            try
            {
                model = (from o in _dbContext.Categories
                         where o.Id == id
                         select new CategoryViewModel
                         {
                             Id = o.Id,
                             Name = o.Name,
                             Active = o.Active,
                             Initial = o.Initial
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new CategoryViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public List<CategoryViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string? orderBy, Sorting sort)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            try
            {
                //filter by search
                //Console.WriteLine(ClaimContext.UserName());
                var query = _dbContext.Categories
                    .Where(o => o.Initial.Contains(search) || o.Name.Contains(search));
                if (query.Count() > 0)
                {
                    switch (orderBy)
                    {
                        case "initial":
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Initial) : query.OrderByDescending(o => o.Initial);
                            break;
                        case "name":
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Name) : query.OrderByDescending(o => o.Name);
                            break;
                        default:
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Id) : query.OrderByDescending(o => o.Id);
                            break;
                    }
                    _result.Data = query.Skip((pageNum - 1) * rows)
                        .Take(rows)
                        .Select(o => new CategoryViewModel
                        {
                            Id = o.Id,
                            Initial = o.Initial,
                            Active = o.Active,
                            Name = o.Name,
                        }).ToList();

                    _result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Success = true;
                    _result.Message = "Not Found";
                }
                
            }
            catch (Exception ex)
            {
                _result.Success = false;
                _result.Message = ex.Message;
            }
            return _result;
        }

        public CategoryViewModel Update(CategoryViewModel model)
        {
            try
            {
                Category entity = _dbContext.Categories
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Initial = model.Initial;
                    entity.Name = model.Name;
                    entity.ModifiedBy = ClaimContext.UserName();
                    entity.ModifiedDate = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }
    }
}
