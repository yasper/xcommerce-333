﻿using Microsoft.EntityFrameworkCore;
using System.Numerics;
using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Security;

namespace XComm.Api.Repositories
{
    public class CartRepositories
    {
        private XCommDbContext _dbContext;
        private ResponseResult _result = new ResponseResult();
        public CartRepositories(XCommDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string? orderBy, Sorting sort)
        {
            List<CartViewModel> result = new List<CartViewModel>();
            try
            {
                //filter by search
                var query = (from prd in _dbContext.Products
                             join crt in _dbContext.Carts on
                             new { v1 = prd.Id, v2 = ClaimContext.UserName() } equals
                             new { v1 = crt.ProductId, v2 = crt.CreatedBy } into prdCrt
                             from crt in prdCrt.DefaultIfEmpty()
                             where prd.Initial.Contains(search) || prd.Name.Contains(search) || prd.Description.Contains(search)
                             select new CartViewModel
                             {
                                 Id = prd.Id,
                                 Initial = prd.Initial,
                                 Name = prd.Name,
                                 Description = prd.Description,
                                 Price = prd.Price,
                                 Stock = prd.Stock,
                                 Active = prd.Active,
                                 Base64 = prd.Gallery.Base64Small,
                                 Quantity = crt.Quantity != null ? crt.Quantity : 0,

                             });
                switch (orderBy)
                {
                    case "initial":
                        query = sort == Sorting.Ascending ? query.OrderBy(o => o.Initial) : query.OrderByDescending(o => o.Initial);
                        break;
                    case "name":
                        query = sort == Sorting.Ascending ? query.OrderBy(o => o.Name) : query.OrderByDescending(o => o.Name);
                        break;
                    case "price":
                        query = sort == Sorting.Ascending ? query.OrderBy(o => o.Price) : query.OrderByDescending(o => o.Price);
                        break;
                    case "stock":
                        query = sort == Sorting.Ascending ? query.OrderBy(o => o.Stock) : query.OrderByDescending(o => o.Stock);
                        break;
                    default:
                        query = sort == Sorting.Ascending ? query.OrderBy(o => o.Id) : query.OrderByDescending(o => o.Id);
                        break;
                }
                _result.Data = query.Skip((pageNum - 1) * rows)
                    .Take(rows).ToList();

                _result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                if (query.Count() > 0)
                {
                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Success = true;
                    _result.Message = "Not Found";
                }
            }
            catch (Exception ex)
            {
                _result.Success = false;
                _result.Message = ex.Message;
            }
            return _result;
        }
    }
}
