﻿using Framework.Auth;
using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Security;

namespace XComm.Api.Repositories
{
    public class AccountRepositories
    {
        private XCommDbContext _dbContext;
        public AccountRepositories(XCommDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public AccountViewModel Authentication(LoginViewModel model)
        {
            AccountViewModel result  = new AccountViewModel();
            Accounts account = _dbContext.Accounts
                .Where(o => o.UserName == model.UserName && o.Password == Encryption.Hashsha256(model.Password)).FirstOrDefault();
            if(account != null)
            {
                result = new AccountViewModel
                {
                    Id = account.Id,
                    UserName = account.UserName,
                    Active = account.Active,
                    FirstName = account.FirstName,
                    Email = account.Email,
                    //Roles = Roles(o.RoleGroupId),
                    LastName = account.LastName,
                };
            }
            result.Roles = Roles(account.RoleGroupId);
            return result;
        }

        public AccountViewModel Authentication(RegisterViewModel model)
        {
            AccountViewModel result = new AccountViewModel();
            try
            {
                Accounts entity = new Accounts();
                entity.Email = model.Email;
                entity.FirstName = model.FirstName;
                entity.LastName = model.LastName;
                entity.UserName = model.UserName;
                entity.Password = Encryption.Hashsha256(model.Password);
                entity.CreatedBy = ClaimContext.UserName();
                entity.CreatedDate = DateTime.Now;
                _dbContext.Accounts.Add(entity);
                _dbContext.SaveChanges();

                model.UserName = entity.UserName;

                result = (from o in _dbContext.Accounts
                          where o.UserName == model.UserName
                          select new AccountViewModel
                          {
                              Id = o.Id,
                              Active = o.Active,
                              Email = o.Email,
                              FirstName= o.FirstName,
                              LastName= o.LastName,
                              Token = o.LastName,
                              UserName = o.UserName
                          }).FirstOrDefault();
                
                result.Roles = Roles(entity.RoleGroupId);
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public List<AccountViewModel> GetAccounts()
        {
            List<AccountViewModel> result = new List<AccountViewModel>();
            try
            {
                result = (from o in _dbContext.Accounts
                          select new AccountViewModel
                          {
                              Id = o.Id,
                              UserName= o.UserName,
                              Active= o.Active,
                              Email = o.Email,
                              FirstName = o.FirstName,
                              LastName = o.LastName
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        private List<string> Roles(long roleGroupId)
        {
            List<string> result = new List<string>();

            var list = _dbContext.AuthorizationGroup
                        .Where(o=> o.RoleGroupId == roleGroupId)
                        .ToList();
            foreach(var g in list)
            {
                result.Add(g.Role);
            }

            return result;
        }
    }
}
