﻿using Microsoft.EntityFrameworkCore;
using System;
using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Security;

namespace XComm.Api.Repositories
{
    public class GalleryRepository : IRepository<GalleryViewModel>
    {
        private XCommDbContext _dbContext;
        private ResponseResult _result = new ResponseResult();
        public GalleryRepository(XCommDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public GalleryViewModel ChangeStatus(long id, bool status)
        {
            throw new NotImplementedException();
        }

        public GalleryViewModel Create(GalleryViewModel model)
        {
            try
            {
                Gallery entity = new Gallery();
                entity.Active = model.Active;
                entity.Title = model.Title;
                entity.Base64Big = model.Base64Big;
                entity.Base64Small = model.Base64Small;
                entity.CreatedBy = ClaimContext.UserName();
                entity.CreatedDate = DateTime.Now;
                _dbContext.Gallery.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;
            }
            catch (Exception)
            {
                throw;
            }

            return model;
        }

        public List<GalleryViewModel> GetAll()
        {
            List<GalleryViewModel> result = new List<GalleryViewModel>();
            try
            {
                result = (from o in _dbContext.Gallery
                          select new GalleryViewModel
                          {
                              Id = o.Id,
                              Base64Big = o.Base64Big,
                              Active = o.Active,
                              Base64Small = o.Base64Small,
                              Title = o.Title,
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public GalleryViewModel GetById(long id)
        {
            GalleryViewModel model = new GalleryViewModel();
            try
            {
                model = (from o in _dbContext.Gallery
                         where o.Id == id
                         select new GalleryViewModel
                         {
                             Id = o.Id,
                             Active = o.Active,
                             Base64Big = o.Base64Big,
                             Base64Small= o.Base64Small,
                             Title = o.Title,
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new GalleryViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<GalleryViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            List<GalleryViewModel> result = new List<GalleryViewModel>();
            try
            {
                //filter by search
                var query = _dbContext.Gallery
                    .Where(o => o.Title.Contains(search));
                if (query.Count()>0)
                {
                    switch (orderBy)
                    {
                        case "title":
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Title) : query.OrderByDescending(o => o.Title);
                            break;
                        default:
                            query = sort == Sorting.Ascending ? query.OrderBy(o => o.Id) : query.OrderByDescending(o => o.Id);
                            break;
                    }
                    _result.Data = query.Skip((pageNum - 1) * rows)
                        .Take(rows)
                        .Select(o => new GalleryViewModel
                        {
                            Id = o.Id,
                            Title = o.Title,
                            Active = o.Active,
                            Base64Big = o.Base64Big,
                            Base64Small = o.Base64Small,
                        }).ToList();

                    _result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Success = true;
                    _result.Message = "Not Found";
                }
                
            }
            catch (Exception ex)
            {
                _result.Success = false;
                _result.Message = ex.Message;
            }
            return _result;
        }

        public GalleryViewModel Update(GalleryViewModel model)
        {
            try
            {
                Gallery entity = _dbContext.Gallery
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Active = model.Active;
                    entity.Base64Big = model.Base64Big;
                    entity.Base64Small = model.Base64Small;
                    entity.Id = model.Id;
                    entity.Title = model.Title;

                    entity.ModifiedBy = ClaimContext.UserName();
                    entity.ModifiedDate = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                    // _result.Data = entity;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }
    }
}
