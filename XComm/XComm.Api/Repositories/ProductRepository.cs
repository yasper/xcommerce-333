﻿using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Security;

namespace XComm.Api.Repositories
{
    public class ProductRepository : IRepository<ProductViewModel>
    {
        private XCommDbContext _dbContext;
        private ResponseResult _result = new ResponseResult();

        public ProductRepository(XCommDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ProductViewModel ChangeStatus(long id, bool status)
        {
            ProductViewModel result = new ProductViewModel();
            try
            {
                Products entity = _dbContext.Products
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Active = status;

                    entity.ModifiedBy = ClaimContext.UserName();
                    entity.ModifiedDate = DateTime.Now;

                    _dbContext.SaveChanges();

                    result = new ProductViewModel()
                    {
                        Active = status,
                        Description = entity.Description,
                        Id = entity.Id,
                        GalleryId = entity.GalleryId,
                        Initial = entity.Initial,
                        Name = entity.Name,
                        Price = entity.Price,
                        Stock = entity.Stock,
                        VariantId = entity.VariantId
                    };
                }
                // else
                // {
                //     _result.Success = false;
                //     _result.Message = "Category Not Found";
                // }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }
        public ProductViewModel ChangeImage(long id, long galleryId)
        {
            ProductViewModel result = new ProductViewModel();
            try
            {
                Products entity = _dbContext.Products
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.GalleryId = galleryId;

                    entity.ModifiedBy = ClaimContext.UserName();
                    entity.ModifiedDate = DateTime.Now;

                    _dbContext.SaveChanges();

                    result = new ProductViewModel()
                    {
                        Active = entity.Active,
                        Description = entity.Description,
                        Id = entity.Id,
                        GalleryId = entity.GalleryId,
                        Initial = entity.Initial,
                        Name = entity.Name,
                        Price = entity.Price,
                        Stock = entity.Stock,
                        VariantId = entity.VariantId
                    };
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public ProductViewModel Create(ProductViewModel model)
        {
            try
            {
                Products entity = new Products();
                entity.Initial = model.Initial;
                entity.Name = model.Name;
                entity.Description = model.Description;
                entity.VariantId = model.VariantId;
                entity.GalleryId = model.GalleryId;
                entity.Price = model.Price;
                entity.Stock = model.Stock;
                entity.Active = model.Active;
                entity.CreatedBy = ClaimContext.UserName();
                entity.CreatedDate = DateTime.Now;
                _dbContext.Products.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;

            }
            catch (Exception)
            {
                throw;
            }
            return model;
        }

        public List<ProductViewModel> GetAll()
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            try
            {
                result = (from o in _dbContext.Products
                          select new ProductViewModel
                          {
                              Id = o.Id,
                              VariantId = o.VariantId,
                              Name = o.Name,
                              Description = o.Description,
                              Variant = new VariantViewModel
                              {
                                  Active = o.Variants.Active,
                                  CategoryId = o.Variants.CategoryId,
                                  Id = o.Variants.Id,
                                  Category = new CategoryViewModel()
                                  {
                                      Active = o.Variants.Category.Active,
                                      Id = o.Variants.Category.Id,
                                      Initial = o.Variants.Category.Initial,
                                      Name = o.Variants.Category.Name
                                  },
                                  Initial = o.Variants.Initial,
                                  Name = o.Variants.Name
                              },
                              Base64 = o.Gallery.Base64Big,
                              GalleryId = o.GalleryId,
                              Price = o.Price,
                              Stock = o.Stock,
                              Active = o.Active,
                              Initial = o.Initial
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public ProductViewModel GetById(long id)
        {
            ProductViewModel model = new ProductViewModel();
            try
            {
                model = (from o in _dbContext.Products
                         where o.Id == id
                         select new ProductViewModel
                         {
                             Id = o.Id,
                             VariantId = o.VariantId,
                             GalleryId = o.GalleryId,
                             Base64 = o.Gallery.Base64Big,
                             Name = o.Name,
                             Description = o.Description,
                             Price = o.Price,
                             Stock = o.Stock,
                             Active = o.Active,
                             Initial = o.Initial
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new ProductViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public List<ProductViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            try
            {
                //filter by search
                var query = _dbContext.Products
                    .Where(o => o.Initial.Contains(search) || o.Name.Contains(search));
                switch (orderBy)
                {
                    case "initial":
                        query = sort == Sorting.Ascending ? query.OrderBy(o => o.Initial) : query.OrderByDescending(o => o.Initial);
                        break;
                    case "name":
                        query = sort == Sorting.Ascending ? query.OrderBy(o => o.Name) : query.OrderByDescending(o => o.Name);
                        break;
                    case "price":
                        query = sort == Sorting.Ascending ? query.OrderBy(o => o.Price) : query.OrderByDescending(o => o.Price);
                        break;
                    case "stock":
                        query = sort == Sorting.Ascending ? query.OrderBy(o => o.Stock) : query.OrderByDescending(o => o.Stock);
                        break;
                    default:
                        query = sort == Sorting.Ascending ? query.OrderBy(o => o.Id) : query.OrderByDescending(o => o.Id);
                        break;
                }
                _result.Data = query.Skip((pageNum - 1) * rows)
                    .Take(rows)
                    .Select(o => new ProductViewModel
                    {
                        Id = o.Id,
                        Initial = o.Initial,
                        Description = o.Description,
                        Price = o.Price,
                        Stock = o.Stock,
                        Variant = new VariantViewModel
                        {
                            Active = o.Variants.Active,
                            CategoryId = o.Variants.CategoryId,
                            Id = o.Variants.Id,
                            Category = new CategoryViewModel()
                            {
                                Active = o.Variants.Category.Active,
                                Id = o.Variants.Category.Id,
                                Initial = o.Variants.Category.Initial,
                                Name = o.Variants.Category.Name
                            },
                            Initial = o.Variants.Initial,
                            Name = o.Variants.Name
                        },
                        Base64 = o.Gallery.Base64Big,
                        GalleryId = o.GalleryId,
                        VariantId = o.VariantId,
                        Active = o.Active,
                        Name = o.Name,
                    }).ToList();

                _result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                if (query.Count() > 0)
                {
                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Success = true;
                    _result.Message = "Not Found";
                }
            }
            catch (Exception ex)
            {
                _result.Success = false;
                _result.Message = ex.Message;
            }
            return _result;
        }

        public ProductViewModel Update(ProductViewModel model)
        {
            try
            {
                Products entity = _dbContext.Products
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Initial = model.Initial;
                    entity.Name = model.Name;
                    entity.Description = model.Description;
                    entity.VariantId = model.VariantId;
                    entity.Price = model.Price;
                    entity.Stock = model.Stock;
                    entity.GalleryId = model.GalleryId;
                    entity.ModifiedBy = ClaimContext.UserName();
                    entity.ModifiedDate = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                    // _result.Data = entity;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }
    }
}
