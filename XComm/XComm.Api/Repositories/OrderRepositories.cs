﻿using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Security;

namespace XComm.Api.Repositories
{
    public class OrderRepositories : IRepository<OrderHeaderViewModel>
    {
        private XCommDbContext _dbContext;

        private ResponseResult _result = new ResponseResult();
        public OrderRepositories(XCommDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public OrderHeaderViewModel Create(OrderHeaderViewModel model)
        {

            try
            {
                string referensi = NewReferrence();
                if (!String.IsNullOrEmpty(referensi))
                {
                    using var trans = _dbContext.Database.BeginTransaction();

                    OrderHeader entity = new OrderHeader();
                    OrderDetail entityDetail = new OrderDetail();
                    entity.Id = model.Id;
                    entity.Reference = referensi;
                    entity.CreatedBy = ClaimContext.UserName();
                    entity.CreatedDate = DateTime.Now;

                    _dbContext.OrderHeaders.Add(entity);
                    _dbContext.SaveChanges();

                    decimal amount = 0;

                    foreach (var item in model.details)
                    {
                        OrderDetail detail = new OrderDetail();
                        detail.HeaderId = entity.Id;
                        detail.ProductId = item.ProductId;
                        detail.Quantity = item.Quantity;
                        detail.Price = item.Price;
                        detail.CreatedBy = ClaimContext.UserName();
                        detail.CreatedDate = DateTime.Now;

                        _dbContext.OrderDetails.Add(detail);

                        amount += item.Price * item.Quantity;
                    }

                    entity.Amount = amount;
                    model.Amount = amount;
                    model.Reference = entity.Reference;

                    _dbContext.OrderHeaders.Update(entity);
                    _dbContext.SaveChanges();

                    trans.Commit();
                }

            }
            catch (Exception)
            {
                throw;
            }

            return model;
        }

        public List<OrderHeaderViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public OrderHeaderViewModel GetById(long id)
        {
            throw new NotImplementedException();
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            throw new NotImplementedException();
        }


        public string NewReferrence()
        {
            //    yyMM-increment
            //SLS-2311-0123
            string yearMonth = DateTime.Now.ToString("yy") + DateTime.Now.Month.ToString("D2");
            string newRef = "SLS-" + yearMonth + "-"; //SLS-2311

            try
            {
                var maxRef = _dbContext.OrderHeaders
                    .Where(o => o.Reference.Contains(newRef))
                    .OrderByDescending(o => o.Reference)
                    .FirstOrDefault();

                if (maxRef != null)
                {
                    //SLS-2311-0002
                    string[] refer = maxRef.Reference.Split('-');
                    int incr = int.Parse(refer[2]) + 1;
                    newRef += incr.ToString("D4");//string denga 4 angka
                }
                else
                {
                    newRef += "0001";
                }
            }
            catch
            {
                newRef = "";
            }
            return newRef;
        }

        public OrderHeaderViewModel Update(OrderHeaderViewModel model)
        {
            throw new NotImplementedException();
        }

        public OrderHeaderViewModel ChangeStatus(long id, bool status)
        {
            throw new NotImplementedException();
        }

        public List<OrderHeaderViewModel> GetByParentId(long parentId)
        {
            throw new NotImplementedException();
        }
    }
}
