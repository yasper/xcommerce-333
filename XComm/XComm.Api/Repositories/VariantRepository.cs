﻿using Microsoft.EntityFrameworkCore;
using ViewModel;
using XComm.Api.DataModel;
using XComm.Api.Security;

namespace XComm.Api.Repositories
{
    public class VariantRepository : IRepository<VariantViewModel>
    {
        private XCommDbContext _dbContext;

        private ResponseResult _result = new ResponseResult();
        public VariantRepository(XCommDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public VariantViewModel ChangeStatus(long id, bool status)
        {
            VariantViewModel result = new VariantViewModel();
            try
            {
                Variants entity = _dbContext.Variants
                    .Where(o => o.Id == id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Active = status;

                    entity.ModifiedBy = ClaimContext.UserName();
                    entity.ModifiedDate = DateTime.Now;

                    _dbContext.SaveChanges();

                    result = new VariantViewModel()
                    {
                        Active = entity.Active,
                        CategoryId = entity.CategoryId,
                        Id = entity.Id,
                        Initial = entity.Initial,
                        Name = entity.Name
                    };
                }
                // else
                // {
                //     _result.Success = false;
                //     _result.Message = "Category Not Found";
                // }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return result;
        }

        public VariantViewModel Create(VariantViewModel model)
        {
            try
            {
                Variants entity = new Variants();
                entity.Name = model.Name;
                entity.Initial = model.Initial;
                entity.Active = model.Active;
                entity.CategoryId = model.CategoryId;
                entity.CreatedBy = ClaimContext.UserName();
                entity.CreatedDate = DateTime.Now;
                _dbContext.Variants.Add(entity);
                _dbContext.SaveChanges();

                model.Id = entity.Id;
            }
            catch (Exception)
            {
                throw;
            }

            return model;
        }


        public List<VariantViewModel> GetAll()
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            try
            {
                result = (from o in _dbContext.Variants
                          select new VariantViewModel
                          {
                              Id = o.Id,
                              CategoryId = o.CategoryId,
                              Name = o.Name,
                              Category = new CategoryViewModel
                              {
                                  Id = o.Category.Id,
                                  Active = o.Category.Active,
                                  Initial = o.Category.Initial,
                                  Name = o.Category.Name
                              },
                              Active = o.Active,
                              Initial = o.Initial
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public VariantViewModel GetById(long id)
        {
            VariantViewModel model = new VariantViewModel();
            try
            {
                model = (from o in _dbContext.Variants
                         where o.Id == id
                         select new VariantViewModel
                         {
                             Id = o.Id,
                             Name = o.Name,
                             CategoryId = o.CategoryId,
                             Active = o.Active,
                             Initial = o.Initial
                         }).FirstOrDefault();
                if (model == null)
                {
                    return new VariantViewModel();
                }
            }
            catch (Exception)
            {
                throw;
            }


            return model;
        }

        public List<VariantViewModel> GetByParentId(long parentId)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            try
            {
                result = (from o in _dbContext.Variants
                          where o.CategoryId == parentId
                          select new VariantViewModel
                          {
                              Id = o.Id,
                              CategoryId = o.CategoryId,
                              Name = o.Name,
                              Category = new CategoryViewModel(){
                                Active = o.Category.Active,
                                Id = o.Category.Id,
                                Initial = o.Category.Initial,
                                Name = o.Category.Name
                              },
                              Active = o.Active,
                              Initial = o.Initial
                          }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        public ResponseResult Pagination(int pageNum, int rows, string search, string orderBy, Sorting sort)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            try
            {
                //filter by search
                var query = _dbContext.Variants
                    .Where(o => o.Initial.Contains(search) || o.Name.Contains(search));
                switch (orderBy)
                {
                    case "initial":
                        query = sort == Sorting.Ascending ? query.OrderBy(o => o.Initial) : query.OrderByDescending(o => o.Initial);
                        break;
                    case "name":
                        query = sort == Sorting.Ascending ? query.OrderBy(o => o.Name) : query.OrderByDescending(o => o.Name);
                        break;
                    default:
                        query = sort == Sorting.Ascending ? query.OrderBy(o => o.Id) : query.OrderByDescending(o => o.Id);
                        break;
                }
                _result.Data = query.Skip((pageNum - 1) * rows)
                    .Take(rows)
                    .Select(o => new VariantViewModel
                    {
                        Id = o.Id,
                        CategoryId = o.CategoryId,
                        Category = new CategoryViewModel
                        {
                            Id = o.Category.Id,
                            Active = o.Category.Active,
                            Initial = o.Category.Initial,
                            Name = o.Category.Name
                        },
                        Initial = o.Initial,
                        Active = o.Active,
                        Name = o.Name,
                    }).ToList();


                _result.Pages = (int)Math.Ceiling((decimal)query.Count() / (decimal)rows);

                if (query.Count() > 0)
                {
                    if (_result.Pages < pageNum)
                    {
                        return Pagination(1, rows, search, orderBy, sort);
                    }
                }
                else
                {
                    _result.Success = true;
                    _result.Message = "Not Found";
                }

            }
            catch (Exception ex)
            {
                _result.Success = false;
                _result.Message = ex.Message;
            }
            return _result;
        }

        public VariantViewModel Update(VariantViewModel model)
        {
            try
            {
                Variants entity = _dbContext.Variants
                    .Where(o => o.Id == model.Id)
                    .FirstOrDefault();

                if (entity != null)
                {
                    entity.Initial = model.Initial;
                    entity.Name = model.Name;
                    entity.CategoryId = model.CategoryId;
                    entity.ModifiedBy = ClaimContext.UserName();
                    entity.ModifiedDate = DateTime.Now;

                    _dbContext.SaveChanges();
                    model.Id = entity.Id;
                    // _result.Data = entity;
                }
                else
                {
                    model.Id = 0;
                    // _result.Success = false;
                    // _result.Message = "Category Not Found";
                    // _result.Data = model;
                }
            }
            catch (Exception e)
            {
                // _result.Success = false;
                // _result.Message = e.Message;
            }
            return model;
        }
    }
}
