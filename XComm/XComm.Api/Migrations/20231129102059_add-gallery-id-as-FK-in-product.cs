﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XComm.Api.Migrations
{
    /// <inheritdoc />
    public partial class addgalleryidasFKinproduct : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9044));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9045));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9046));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9048));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 5L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9049));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 6L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9050));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "Admin",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(8372));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(8387));

            migrationBuilder.UpdateData(
                table: "RoleGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9010));

            migrationBuilder.UpdateData(
                table: "RoleGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9015));

            migrationBuilder.CreateIndex(
                name: "IX_MasterProducts_GalleryId",
                table: "MasterProducts",
                column: "GalleryId");

            migrationBuilder.AddForeignKey(
                name: "FK_MasterProducts_Galleries_GalleryId",
                table: "MasterProducts",
                column: "GalleryId",
                principalTable: "Galleries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MasterProducts_Galleries_GalleryId",
                table: "MasterProducts");

            migrationBuilder.DropIndex(
                name: "IX_MasterProducts_GalleryId",
                table: "MasterProducts");

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(843));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(845));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(846));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(847));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 5L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(849));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 6L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(850));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "Admin",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(554));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(566));

            migrationBuilder.UpdateData(
                table: "RoleGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(774));

            migrationBuilder.UpdateData(
                table: "RoleGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(821));
        }
    }
}
