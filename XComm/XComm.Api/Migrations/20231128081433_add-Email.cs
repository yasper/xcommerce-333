﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XComm.Api.Migrations
{
    /// <inheritdoc />
    public partial class addEmail : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "MasterAccounts",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: false,
                defaultValue: "auriwanyasper@gmail.com");

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "Admin",
                columns: new[] { "CreatedDate", "Email" },
                values: new object[] { new DateTime(2023, 11, 28, 15, 14, 33, 296, DateTimeKind.Local).AddTicks(7278), "auriwanyasper@gmail.com" });

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                columns: new[] { "CreatedDate", "Email" },
                values: new object[] { new DateTime(2023, 11, 28, 15, 14, 33, 296, DateTimeKind.Local).AddTicks(7288), "auriwanyasper@gmail.com" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "MasterAccounts");

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "Admin",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 28, 14, 47, 0, 36, DateTimeKind.Local).AddTicks(2230));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 28, 14, 47, 0, 36, DateTimeKind.Local).AddTicks(2247));
        }
    }
}
