﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XComm.Api.Migrations
{
    /// <inheritdoc />
    public partial class galleryidnullable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MasterProducts_Galleries_GalleryId",
                table: "MasterProducts");

            migrationBuilder.AlterColumn<long>(
                name: "GalleryId",
                table: "MasterProducts",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldDefaultValue: 1L);

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 4, 13, 48, 34, 997, DateTimeKind.Local).AddTicks(7328));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 4, 13, 48, 34, 997, DateTimeKind.Local).AddTicks(7331));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 4, 13, 48, 34, 997, DateTimeKind.Local).AddTicks(7332));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 4, 13, 48, 34, 997, DateTimeKind.Local).AddTicks(7334));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 5L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 4, 13, 48, 34, 997, DateTimeKind.Local).AddTicks(7336));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 6L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 4, 13, 48, 34, 997, DateTimeKind.Local).AddTicks(7338));

            migrationBuilder.InsertData(
                table: "AuthorizationGroups",
                columns: new[] { "Id", "Active", "CreatedBy", "CreatedDate", "ModifiedBy", "ModifiedDate", "Role", "RoleGroupId" },
                values: new object[] { 7L, false, "admin", new DateTime(2023, 12, 4, 13, 48, 34, 997, DateTimeKind.Local).AddTicks(7339), null, null, "Galleries", 2L });

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "Admin",
                columns: new[] { "CreatedDate", "RoleGroupId" },
                values: new object[] { new DateTime(2023, 12, 4, 13, 48, 34, 997, DateTimeKind.Local).AddTicks(7054), 2L });

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2023, 12, 4, 13, 48, 34, 997, DateTimeKind.Local).AddTicks(7067));

            migrationBuilder.UpdateData(
                table: "RoleGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 4, 13, 48, 34, 997, DateTimeKind.Local).AddTicks(7295));

            migrationBuilder.UpdateData(
                table: "RoleGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 12, 4, 13, 48, 34, 997, DateTimeKind.Local).AddTicks(7298));

            migrationBuilder.AddForeignKey(
                name: "FK_MasterProducts_Galleries_GalleryId",
                table: "MasterProducts",
                column: "GalleryId",
                principalTable: "Galleries",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MasterProducts_Galleries_GalleryId",
                table: "MasterProducts");

            migrationBuilder.DeleteData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 7L);

            migrationBuilder.AlterColumn<long>(
                name: "GalleryId",
                table: "MasterProducts",
                type: "bigint",
                nullable: false,
                defaultValue: 1L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9044));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9045));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9046));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9048));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 5L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9049));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 6L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9050));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "Admin",
                columns: new[] { "CreatedDate", "RoleGroupId" },
                values: new object[] { new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(8372), 1L });

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(8387));

            migrationBuilder.UpdateData(
                table: "RoleGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9010));

            migrationBuilder.UpdateData(
                table: "RoleGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 59, 295, DateTimeKind.Local).AddTicks(9015));

            migrationBuilder.AddForeignKey(
                name: "FK_MasterProducts_Galleries_GalleryId",
                table: "MasterProducts",
                column: "GalleryId",
                principalTable: "Galleries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
