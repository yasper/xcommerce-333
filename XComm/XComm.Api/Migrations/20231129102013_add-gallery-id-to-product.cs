﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace XComm.Api.Migrations
{
    /// <inheritdoc />
    public partial class addgalleryidtoproduct : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "GalleryId",
                table: "MasterProducts",
                type: "bigint",
                nullable: false,
                defaultValue: 1L);

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(843));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(845));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(846));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(847));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 5L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(849));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 6L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(850));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "Admin",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(554));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(566));

            migrationBuilder.UpdateData(
                table: "RoleGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(774));

            migrationBuilder.UpdateData(
                table: "RoleGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 17, 20, 13, 471, DateTimeKind.Local).AddTicks(821));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GalleryId",
                table: "MasterProducts");

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 16, 32, 2, 678, DateTimeKind.Local).AddTicks(6688));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 16, 32, 2, 678, DateTimeKind.Local).AddTicks(6690));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 16, 32, 2, 678, DateTimeKind.Local).AddTicks(6691));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 16, 32, 2, 678, DateTimeKind.Local).AddTicks(6693));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 5L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 16, 32, 2, 678, DateTimeKind.Local).AddTicks(6694));

            migrationBuilder.UpdateData(
                table: "AuthorizationGroups",
                keyColumn: "Id",
                keyValue: 6L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 16, 32, 2, 678, DateTimeKind.Local).AddTicks(6696));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "Admin",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 16, 32, 2, 678, DateTimeKind.Local).AddTicks(6342));

            migrationBuilder.UpdateData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1",
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 16, 32, 2, 678, DateTimeKind.Local).AddTicks(6390));

            migrationBuilder.UpdateData(
                table: "RoleGroups",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 16, 32, 2, 678, DateTimeKind.Local).AddTicks(6660));

            migrationBuilder.UpdateData(
                table: "RoleGroups",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreatedDate",
                value: new DateTime(2023, 11, 29, 16, 32, 2, 678, DateTimeKind.Local).AddTicks(6661));
        }
    }
}
