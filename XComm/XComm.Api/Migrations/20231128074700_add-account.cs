﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace XComm.Api.Migrations
{
    /// <inheritdoc />
    public partial class addaccount : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "MasterAccounts",
                columns: new[] { "UserName", "Active", "CreatedBy", "CreatedDate", "FirstName", "Id", "LastName", "ModifiedBy", "ModifiedDate", "Password" },
                values: new object[,]
                {
                    { "Admin", true, "Admin", new DateTime(2023, 11, 28, 14, 47, 0, 36, DateTimeKind.Local).AddTicks(2230), "Super", 1L, "Admin", null, null, "ac9689e2272427085e35b9d3e3e8bed88cb3434828b43b86fc0596cad4c6e270" },
                    { "user1", true, "Admin", new DateTime(2023, 11, 28, 14, 47, 0, 36, DateTimeKind.Local).AddTicks(2247), "User", 2L, "One", null, null, "0a041b9462caa4a31bac3567e0b6e6fd9100787db2ab433d96f6d178cabfce90" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "Admin");

            migrationBuilder.DeleteData(
                table: "MasterAccounts",
                keyColumn: "UserName",
                keyValue: "user1");
        }
    }
}
