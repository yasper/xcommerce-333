import axios from "axios";
import { IPagination } from "../interfaces/iPagination";
import { config } from "../configuration/config";

export const CartService = {
  getCart: (pg: IPagination) => {
    const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : "";
    const result = axios
      .get(
        config.apiUrl +
          `/Cart?pageNum=${pg.pageNum}&rows=${pg.rows}&orderBy=${pg.orderBy}&sort=${pg.sort}${searchStr}`,
        {
          headers: config.headers(),
        }
      )

      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },
};
