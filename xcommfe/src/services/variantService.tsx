import axios from "axios";
import { config } from "../configuration/config";
import { iVariant } from "../interfaces/iVariant";
import { IPagination } from "../interfaces/iPagination";
import { log } from "console";

export const VariantService = {
  getAll: (pg: IPagination) => {
    const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : "";
    const result = axios
      .get(
        config.apiUrl +
          `/Variant?pageNum=${pg.pageNum}&rows=${pg.rows}&orderBy=${pg.orderBy}&sort=${pg.sort}${searchStr}`
      )

      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },
  getAllNoPag: () => {
    const result = axios
      .get(config.apiUrl + `/Variant/getAll`)

      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
  post: (variant: iVariant) => {
    const { category, ...newVariant } = variant;
    const result = axios
      .post(config.apiUrl + "/Variant", newVariant, {
        headers: config.headers(),
      })
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  getById: (id: number) => {
    const result = axios
      .get(config.apiUrl + "/Variant/" + id)
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  getByParentId: (id: number) => {
    //id => categoryId
    console.log(config.apiUrl + "/Variant/category/" + id);

    const result = axios
      .get(config.apiUrl + "/Variant/category/" + id)
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
  update: (id: number, variant: iVariant) => {
    const result = axios
      .put(config.apiUrl + "/Variant/" + id, variant, {
        headers: config.headers(),
      })
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
  changeStatus: (id: number, variant: iVariant) => {
    const result = axios
      .put(
        config.apiUrl +
          "/Variant/changestatus/" +
          id +
          "?status=" +
          variant.active,
        null,
        {
          headers: config.headers(),
        }
      )
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });

    return result;
  },
};
