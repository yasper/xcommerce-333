import axios from "axios";
import { config } from "../configuration/config";
import { iCategory } from "../interfaces/iCategory";
import { IPagination } from "../interfaces/iPagination";

export const CategoryService = {
  getAll: (pg: IPagination) => {
    const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : "";
    const result = axios
      .get(
        config.apiUrl +
          `/Category?pageNum=${pg.pageNum}&rows=${pg.rows}${searchStr}&orderBy=${pg.orderBy}&sort=${pg.sort}`
      )
      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },

  getAllNoPag: () => {
    const result = axios
      .get(`${config.apiUrl}/Category/getAll`)
      .then((respons) => {
        return {
          success: respons.status == 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },

  post: (category: iCategory) => {
    const result = axios
      .post(config.apiUrl + "/Category", category, {
        headers: config.headers(),
      })
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  getById: (id: number) => {
    const result = axios
      .get(config.apiUrl + "/Category/" + id)
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  update: (id: number, category: iCategory) => {
    const result = axios
      .put(config.apiUrl + "/Category/" + id, category, {
        headers: config.headers(),
      })
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
  changeStatus: (id: number, category: iCategory) => {
    const result = axios
      .put(
        config.apiUrl +
          "/Category/changestatus/" +
          id +
          "?status=" +
          category.active,
        null,
        {
          headers: config.headers(),
        }
      )
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });

    return result;
  },
};
