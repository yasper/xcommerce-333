import axios from "axios";
import { IPagination } from "../interfaces/iPagination";
import { config } from "../configuration/config";
import { IGallery } from "../interfaces/iGallery";

export const GalleryService = {
  getAll: (pg: IPagination) => {
    const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : "";
    const result = axios
      .get(
        config.apiUrl +
          `/Gallery?pageNum=${pg.pageNum}&rows=${pg.rows}${searchStr}&orderBy=${pg.orderBy}&sort=${pg.sort}`
      )
      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },
  post: (gallery: IGallery) => {
    const result = axios
      .post(config.apiUrl + "/Gallery", gallery, {
        headers: config.headers(),
      })
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  getAllNoPag: () => {
    const result = axios
      .get(config.apiUrl + `/Gallery/getAll`)

      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
  update: (id: number, gal: IGallery) => {
    const result = axios
      .put(config.apiUrl + "/Gallery/" + id, gal, {
        headers: config.headers(),
      })
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
  getById: (id: number) => {
    const result = axios
      .get(config.apiUrl + "/Gallery/" + id)
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
};
