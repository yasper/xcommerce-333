import axios from "axios";
import { OrderModel } from "../components/models/orderModel";
import { config } from "../configuration/config";
import { DetailModel } from "../components/models/detailModel";

export const OrderService = {
  post: (details: DetailModel[]) => {
    let newDetails: any = [];
    let amount: number = 0;

    details.map((o) => {
      const { product, ...exProduct } = o;
      newDetails.push(exProduct);
      amount += o.price * o.quantity;
    });

    console.log({ amount: amount, newDetails });

    let order = { amount: amount, reference: "", details: newDetails };

    const result = axios
      .post(config.apiUrl + "/Order", order)

      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
};
