import axios from "axios";
import { config } from "../configuration/config";
import { iProduct } from "../interfaces/iProduct";
import { IPagination } from "../interfaces/iPagination";

export const ProductService = {
  getAll: (pg: IPagination) => {
    const searchStr = pg.search.length > 0 ? `&search=${pg.search}` : "";
    const result = axios
      .get(
        config.apiUrl +
          `/Product?pageNum=${pg.pageNum}&rows=${pg.rows}&orderBy=${pg.orderBy}&sort=${pg.sort}${searchStr}`,
        {
          headers: config.headers(),
        }
      )

      .then((respons) => {
        return {
          success: respons.data.success,
          result: respons.data.data,
          pages: respons.data.pages,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
          pages: 0,
        };
      });
    return result;
  },
  post: (prod: iProduct) => {
    const { galleryId, variant, ...newProduct } = prod;
    const result = axios
      .post(config.apiUrl + "/Product", newProduct, {
        headers: config.headers(),
      })
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  getById: (id: number) => {
    const result = axios
      .get(config.apiUrl + "/Product/" + id, {
        headers: config.headers(),
      })
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },

  update: (id: number, product: iProduct) => {
    const { variant, ...newProduct } = product;
    const result = axios
      .put(config.apiUrl + "/Product/" + id, newProduct, {
        headers: config.headers(),
      })
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });
    return result;
  },
  changeStatus: (id: number, variant: boolean) => {
    const result = axios
      .put(
        config.apiUrl + "/Product/changestatus/" + id + "?status=" + variant,
        null,
        {
          headers: config.headers(),
        }
      )
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });

    return result;
  },

  changeImage: (id: number, galleryId: number) => {
    const result = axios
      .put(
        config.apiUrl + "/Product/changeimage/" + id + "/" + galleryId,
        null,
        {
          headers: config.headers(),
        }
      )
      .then((respons) => {
        return {
          success: respons.status === 200,
          result: respons.data,
        };
      })
      .catch((error) => {
        return {
          success: false,
          result: error,
        };
      });

    return result;
  },
};
