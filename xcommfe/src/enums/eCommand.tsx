export enum ECommand {
    create = 'Create',
    edit = 'Edit',
    changeStatus = 'Change Status',
    addImage = 'Add Image'
}