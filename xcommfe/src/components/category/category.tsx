import React from "react";

import { iCategory } from "../../interfaces/iCategory";
import { CategoryService } from "../../services/categoryServices";

import Form from "./form";
import { ECommand } from "../../enums/eCommand";
import { IPagination } from "../../interfaces/iPagination";
import { config } from "../../configuration/config";

interface IProps {}

interface IState {
  categories: iCategory[];
  category: iCategory;
  showModal: boolean;
  commad: ECommand;
  pagination: IPagination;
}

export default class Category extends React.Component<IProps, IState> {
  newPagination: IPagination = {
    pageNum: 1,
    rows: config.rowPerPage[0],
    search: "",
    orderBy: "",
    sort: 0,
    pages: 0,
  };
  constructor(props: IProps) {
    super(props);
    this.state = {
      categories: [],
      category: {
        id: 0,
        initial: "",
        name: "",
        active: false,
      },
      showModal: false,
      commad: ECommand.create,
      pagination: this.newPagination,
    };
  }

  componentDidMount(): void {
    this.loadCategories();
  }

  loadCategories = async () => {
    const { pagination } = this.state;
    const result = await CategoryService.getAll(pagination);
    if (result.success) {
      this.setState({
        categories: result.result,
        pagination: {
          ...this.state.pagination,
          pages: result.pages,
        },
      });
    } else {
      alert("Error: " + result.result);
    }
  };

  setShowModal = (val: boolean) => {
    this.setState({
      showModal: val,
    });
    console.log(this.state.showModal);
  };

  changeHandler = (name: any) => (event: any) => {
    this.setState({
      category: {
        ...this.state.category,
        [name]: event.target.value,
      },
    });
  };

  checkBoxHandler = (name: any) => (event: any) => {
    this.setState({
      category: {
        ...this.state.category,
        [name]: event.target.checked,
      },
    });
  };

  creatCommand = (command: string) => {
    this.setState({
      commad: ECommand.create,
      category: { id: 0, active: false, initial: "", name: "" },
      showModal: true,
    });
    // this.setShowModal(true)
  };

  updateCommand = async (id: number) => {
    await CategoryService.getById(id)
      .then((result) => {
        if (result.success) {
          this.setState({
            showModal: true,
            commad: ECommand.edit,
            category: result.result,
          });
          this.loadCategories();
        } else {
          alert("Error result: " + result.result);
        }
      })
      .catch((error) => {
        alert("Error error: " + error);
      });
  };

  changeStatusCommand = async (id: number) => {
    await CategoryService.getById(id)
      .then((result) => {
        if (result.success) {
          this.setState({
            showModal: true,
            commad: ECommand.changeStatus,
            category: result.result,
          });
          this.loadCategories();
        } else {
          alert("Error result: " + result.result);
        }
      })
      .catch((error) => {
        alert("Error error: " + error);
      });
  };

  changeRowPerPage = (name: any) => (event: any) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        [name]: event.target.value,
      },
    });
    new Promise(() => {
      setTimeout(() => {
        this.loadCategories();
      }, 500);
    });
  };

  changeSearch = (name: any) => (event: any) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        [name]: event.target.value,
      },
    });
  };

  changeOrder = (order: string) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        orderBy: order,
      },
    });
    new Promise(() => {
      setTimeout(() => {
        this.loadCategories();
      }, 500);
    });
  };

  //menggunakan promise
  submitHandler = async () => {
    const { commad } = this.state;
    if (commad === ECommand.create) {
      await CategoryService.post(this.state.category)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              category: {
                id: 0,
                initial: "",
                name: "",
                active: true,
              },
            });
            this.loadCategories();
          } else {
            alert("Error result: " + result.result);
          }
        })
        .catch((error) => {
          alert("Error error: " + error);
        });
    } else if (commad === ECommand.edit) {
      await CategoryService.update(this.state.category.id, this.state.category)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              category: {
                id: 0,
                initial: "",
                name: "",
                active: true,
              },
            });
            this.loadCategories();
          } else {
            alert("Error result: " + result.result);
          }
        })
        .catch((error) => {
          alert("Error error: " + error);
        });
    } else if (commad === ECommand.changeStatus) {
      await CategoryService.changeStatus(
        this.state.category.id,
        this.state.category
      )
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              category: {
                id: 0,
                initial: "",
                name: "",
                active: true,
              },
            });
            this.loadCategories();
          } else {
            alert("Error result: " + result.result);
          }
        })
        .catch((error) => {
          alert("Error error: " + error);
        });
    }
  };

  render() {
    const { categories, category, showModal, commad, pagination } = this.state;
    const loopPages = () => {
      let content: any = [];
      for (let page = 1; page <= pagination.pages; page++) {
        content.push(<option value={page}>{page}</option>);
      }
      return content;
    };
    return (
      <div>
        <div className="text-left text-3xl pt-5">Categories</div>
        <span>{JSON.stringify(category)}</span>
        <div className="flex" aria-label="Button">
          <button
            className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded focus:shadow-outline hover:bg-green-800"
            onClick={() => this.creatCommand("create")}
          >
            Create New
          </button>
        </div>
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
              <th colSpan={1} scope="col" className="px-6 py-3 w-14 h-14">
                {/* grow flex-none  */}
                Search
              </th>
              <th colSpan={2} scope="col" className="px-6 py-3 w-14 h-14">
                <input
                  type="text"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  required
                  value={pagination.search}
                  onChange={this.changeSearch("search")}
                />
              </th>
              <th colSpan={1} scope="col" className="px-6 py-3 w-14 h-14">
                <button
                  className="h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg rounded-r-lg focus:shadow-outline hover:bg-green-800"
                  onClick={() => this.loadCategories()}
                >
                  Filter
                </button>
              </th>
              <th colSpan={1} scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowPerPage("sort")}
                >
                  <option value="0">Asc</option>
                  <option value="1">Desc</option>
                </select>
              </th>
            </tr>
            <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14"
                onClick={() => this.changeOrder("")}
              >
                {/* grow flex-none  */}
                Id
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14"
                onClick={() => this.changeOrder("initial")}
              >
                Initial
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14"
                onClick={() => this.changeOrder("name")}
              >
                Name
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Active
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {categories?.map((cat: iCategory) => {
              return (
                <tr
                  key={cat.id}
                  className="border-b dark:bg-gray-800 dark:border-gray-700"
                >
                  <td
                    scope="row"
                    className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                  >
                    {cat.id}
                  </td>
                  <td className="px-6 py-4">{cat.initial}</td>
                  <td className="px-6 py-4">{cat.name}</td>
                  <td className="px-6 py-4">
                    <div className="flex items-center">
                      <input
                        checked={cat.active}
                        id="checked-checkbox"
                        type="checkbox"
                        className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                      />
                    </div>
                  </td>
                  <td className="px-4 py-4">
                    <div
                      className="inline-flex"
                      role="group"
                      aria-label="Button group"
                    >
                      <button
                        className="h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg focus:shadow-outline hover:bg-green-800"
                        onClick={() => this.updateCommand(cat.id)}
                      >
                        Edit
                      </button>
                      <button
                        className="h-8 px-4 text-blue-100 transition-colors duration-150 bg-blue-700 rounded-r-lg focus:shadow-outline hover:bg-blue-800"
                        onClick={() => this.changeStatusCommand(cat.id)}
                      >
                        Status
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
          <tfoot className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <label className="block text-sm font-medium text-gray-900 dark:text-white">
                  Rows per page
                </label>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowPerPage("rows")}
                >
                  {config.rowPerPage.map((o: number) => {
                    return <option value={o}>{o}</option>;
                  })}
                </select>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14"></th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Page:
              </th>
              <th colSpan={3} scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowPerPage("pageNum")}
                >
                  {loopPages()}
                </select>
              </th>
            </tr>
          </tfoot>
        </table>
        {showModal ? (
          <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none ">
            <div className="relative w-auto my-6 mx-auto max-w-3xl ">
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none dark:bg-gray-900">
                <div className="flex items-start justify-between p-5 border-b border-solid border-gray-300 rounded-t ">
                  <h3 className="text-3xl text-gray-900 dark:text-white">
                    {commad.valueOf()}
                  </h3>
                  <button
                    className="bg-transparent border-0 text-black float-right"
                    onClick={() => this.setShowModal(false)}
                  >
                    <span className="text-black opacity-7 h-6 w-6 text-xl block bg-gray-400 py-0 rounded-full">
                      x
                    </span>
                  </button>
                </div>
                <div className="relative p-6 flex-auto">
                  <Form
                    command={commad}
                    category={category}
                    changeHandler={this.changeHandler}
                    checkBoxHandler={this.checkBoxHandler}
                  />
                </div>
                <div
                  className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b"
                  role="group"
                  aria-label="Button group"
                >
                  <button
                    className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg focus:shadow-outline hover:bg-green-800"
                    onClick={() => this.setShowModal(false)}
                  >
                    Close
                  </button>
                  <button
                    className="my-8 justify-start h-8 px-4 text-blue-100 transition-colors duration-150 bg-blue-700 rounded-r-lg focus:shadow-outline hover:bg-blue-800"
                    onClick={() => this.submitHandler()}
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}
