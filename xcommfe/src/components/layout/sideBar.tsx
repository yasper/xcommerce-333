import React from "react";
import { Link, Route, Routes } from "react-router-dom";

import { Home } from "../home";
import { Category } from "../category";
import { Variant } from "../variant";
import { Product } from "../product";
import { Gallery } from "../gallery";
import { Authentication } from "../auth";
import { ProtectedRoute } from "./protectedRoute";
import { AuthService } from "../../services/authService";
import Order from "../order/order";
import { AccountModel } from "../models/accountModel";
import Header from "./header";
import { Restricted } from "./restricted";

interface IProps {
  logged: boolean;
  changeLoggedHandler: any;
  account: AccountModel;
}
interface IState {}

export default class SideBar extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
  }

  render() {
    const { logged, account, changeLoggedHandler } = this.props;
    return (
      <>
        <Header account={account} logged={logged} />
        <div className="w-full h-screen flex aBO">
          <div className="bg-slate-800 text-white h-auto w-64 px-4 py-8">
            <ul className="mt-8">
              <li className="rounded-sm">
                <a
                  href="#"
                  className="flex items-center p-2 space-x-3 rounded-md"
                >
                  <span className="text-gray-100">
                    <Link to="/">Home</Link>
                  </span>
                </a>
              </li>
              {logged ? (
                <>
                  {account.roles.map((o) => {
                    return (
                      <li className="rounded-sm">
                        <a
                          href="#"
                          className="flex items-center p-2 space-x-3 rounded-md"
                        >
                          <span className="text-gray-100">
                            <Link to={`/${o}`}>{o}</Link>
                          </span>
                        </a>
                      </li>
                    );
                  })}
                </>
              ) : null}

              <li className="rounded-sm">
                <a
                  href="#"
                  className="flex items-center p-2 space-x-3 rounded-md"
                >
                  <span className="text-gray-100">
                    <Link to="/Authentication">
                      {logged ? "Log Out" : "Log In"}
                    </Link>
                  </span>
                </a>
              </li>
            </ul>
          </div>
          <div className="w-full h-full grid grid-cols-1 gap-3 px-8 text-center">
            <Routes>
              <Route path="/" Component={Home} />
              <Route
                element={
                  <ProtectedRoute
                    isAllowed={account.roles.indexOf("Categories") > -1}
                  />
                }
              >
                <Route path="/Categories" element={<Category />} />
              </Route>
              <Route
                element={
                  <ProtectedRoute
                    isAllowed={account.roles.indexOf("Variants") > -1}
                  />
                }
              >
                <Route path="/Variants" element={<Variant />} />
              </Route>
              <Route
                element={
                  <ProtectedRoute
                    isAllowed={account.roles.indexOf("Products") > -1}
                  />
                }
              >
                <Route path="/Products" element={<Product />} />
              </Route>
              <Route
                element={
                  <ProtectedRoute
                    isAllowed={account.roles.indexOf("Galleries") > -1}
                  />
                }
              >
                <Route path="/Galleries" element={<Gallery />} />
              </Route>
              <Route
                element={
                  <ProtectedRoute
                    isAllowed={account.roles.indexOf("Orders") > -1}
                  />
                }
              >
                <Route path="/Orders" element={<Order />} />
              </Route>
              <Route
                path="/Authentication"
                element={
                  <Authentication
                    logged={logged}
                    changeLoggedHandler={changeLoggedHandler}
                  />
                }
              />
              <Route path="/restricted" Component={Restricted} />
            </Routes>
          </div>
        </div>
      </>
    );
  }
}
