import { Navigate, Outlet } from "react-router-dom";
import { AuthService } from "../../services/authService";

export const ProtectedRoute = ({ isAllowed }: any) => {
  return AuthService.getToken() ? (isAllowed ? (<Outlet />) : (<Navigate to="/restricted" />)) : (<Navigate to="/Authentication" />);
};
