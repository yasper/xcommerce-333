import React from "react";
import SideBar from "./sideBar";
import { RxAvatar } from "react-icons/rx";
import { AccountModel } from "../models/accountModel";

interface IProps {
  logged: boolean;
  account: AccountModel;
}

interface IState {}

export default class Header extends React.Component<IProps, IState> {
  render() {
    const { account, logged } = this.props;
    return (
      <div>
        <div className="bg-gray-800 p-3 flex flex-col lg:flex-row list-none lg:ml-auto">
          <div className="container mx-auto space-y-3">
            <h2 className="text-xl font-bold text-white text-left">XCommerce</h2>
          </div>
          <div className="flex items-center text-green-100 transition-colors duration-150 rounded focus:shadow-outline hover:bg-slate-800">
            <h2 className="text-sm text-white text-right">
              Hi, {logged ? account.userName : "Guest"}
            </h2>
            <RxAvatar className="w-8 h-8 ms-6"/>
          </div>
        </div>
      </div>
    );
  }
}
