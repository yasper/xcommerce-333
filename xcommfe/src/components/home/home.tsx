import React from "react";
import { config } from "../../configuration/config";
import { iVariant } from "../../interfaces/iVariant";
import { iCategory } from "../../interfaces/iCategory";
import { IPagination } from "../../interfaces/iPagination";
import { AiOutlineShoppingCart } from "react-icons/ai";
import { CartService } from "../../services/cartService";
import { IScriptSnapshot } from "typescript";
import { CartModel } from "../models/cartModel";

interface IProps {}
interface IState {
  cart: CartModel;
  carts: CartModel[];
  pagination: IPagination;
  categories: iCategory[];
  noImage: any;
}
export default class Home extends React.Component<IProps, IState> {
  newPagination: IPagination = {
    pageNum: 1,
    rows: 8,
    search: "",
    orderBy: "id",
    sort: 1,
    pages: 0,
  };
  newcart: CartModel = {
    id: 0,
    galleryId: 0,
    initial: "",
    name: "",
    base64: "",
    description: "",
    price: 0,
    stock: 0,
    quantity: 0,
  };
  constructor(props: IProps) {
    super(props);
    this.state = {
      cart: this.newcart,
      carts: [],
      categories: [],
      pagination: this.newPagination,
      noImage: config.noImage,
    };
  }
  componentDidMount(): void {
    this.loadCarts();
  }

  loadCarts = async () => {
    const { pagination } = this.state;
    const result = await CartService.getCart(pagination);
    if (result.success) {
      this.setState({
        carts: result.result,
        pagination: {
          ...this.state.pagination,
          pages: result.pages,
        },
      });
    } else {
      alert("Error: " + result.result);
    }
  };

  changePage = (direction: any) => {
    const { pagination } = this.state;
    let newPageNum = pagination.pageNum;

    if (direction === "prev") {
      newPageNum -= 1;
      if (newPageNum < 1) {
        newPageNum = pagination.pages;
      }
    } else if (direction === "next") {
      newPageNum += 1;
      if (newPageNum > pagination.pages) {
        newPageNum = 1;
      }
    }

    this.setState(
      {
        pagination: {
          ...pagination,
          pageNum: newPageNum,
        },
      },
      () => {
        this.loadCarts();
      }
    );
  };

  render() {
    const { pagination, carts, noImage } = this.state;
    return (
      <div>
        <div className="flex max-w-full w-full p-2 justify-center h-screen overflow-y-scroll ">
          <div className="grid grid-cols-4 md:grid-cols-4 gap-4 items-center h-auto max-w-full rounded-lg">
            {carts.map((o: CartModel) => {
              return (
                <div key={o.id}>
                  <img
                    className="h-auto rounded-lg mx-auto"
                    src={o.base64 ? o.base64 : noImage}
                    alt="Kosong"
                    width="128"
                    height="128"
                  />
                  <div className="flex-col p-5 text-center">
                    <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 ">
                      {o.name}
                    </h5>
                    <h5 className="mb-2 text-lg font-light tracking-tight text-gray-900 ">
                      Rp.{o.price}
                    </h5>
                    <p className="mb-3 font-normal text-gray-600 dark:text-gray-400">
                      {o.description}
                    </p>
                  </div>
                  <div className="flex justify-center items-center my-8 mr-6 h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded focus:shadow-outline hover:bg-green-800">
                    <p className="font-normal text-white-600 dark:text-white-400">
                      {o.quantity}
                    </p>
                    <AiOutlineShoppingCart className="ms-2" />
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="p-2 font-medium text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400 my-2">
          <button
            className={`text-lg mx-2 px-2 py-1 border text-white-500 rounded-md focus:outline-none focus:ring focus:border-blue-300 ${
              pagination.pages <= 1 ? "hidden" : "visible"
            }`}
            onClick={() => this.changePage("prev")}
          >
            Previous
          </button>
          <span className="text-slate-800">{pagination.pageNum}</span>
          <button
            className={`text-lg mx-2 px-2 py-1 border text-slate-500 rounded-md focus:outline-none focus:ring focus:border-blue-300 ${
              pagination.pages !== 1 ? "visible" : "hidden"
            }`}
            onClick={() => this.changePage("next")}
          >
            Next
          </button>
        </div>
      </div>
    );
  }
}
