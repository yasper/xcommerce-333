import React from "react";
import { buttonClass, inputClass } from "../style/styleComponent";
import { iProduct } from "../../interfaces/iProduct";
import ProductList from "./productList";
import { ProductService } from "../../services/productServices";
import { DetailModel } from "../models/detailModel";
import { ProductModel } from "../models/productModel";
import { config } from "../../configuration/config";
import { OrderService } from "../../services/orderService";
import { error } from "console";

interface IProps {}
interface IState {
  showModal: boolean;
  details: DetailModel[];
  products: iProduct[];
}

export default class Order extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      showModal: false,
      products: [],
      details: [],
    };
  }
  newItem = () => {
    this.setState({
      showModal: true,
    });
  };

  setShowModal = (val: boolean) => {
    this.setState({
      showModal: val,
    });
  };

  selectProduct = (id: number) => {
    ProductService.getById(id).then((result) => {
      if (result.success) {
        const product: ProductModel = result.result;
        const item: DetailModel = {
          product: {
            id: product.id,
            base64: product.base64,
            price: product.price,
            stock: product.stock,
            description: product.description,
            galleryId: product.galleryId,
            initial: product.initial,
            name: product.name,
          },
          productId: product.id,
          price: product.price,
          quantity: 1,
        };
        const { details } = this.state;
        details.push(item);
        this.setState({
          details: details,
          showModal: false,
        });
      }
    });
  };

  removeProduct = (idx: number) => {
    const { details } = this.state;
    details.splice(idx, 1);
    this.setState({
      details: details,
    });
  };

  changeHandler = (idx: number) => (event: any) => {
    const { details } = this.state;
    details[idx].quantity = event.target.value;
    this.setState({
      details: details,
    });
  };

  submitHandler = () => {
    OrderService.post(this.state.details)
      .then((result) => {
        if (result.success) {
          this.setState({
            details: [],
          });
        }
      })
      .catch((error) => {
        alert("error:" + error);
      });
  };

  render() {
    const { showModal, products, details } = this.state;
    return (
      <>
        <div>
          {JSON.stringify({
            amount: details.reduce(
              (a, b) => a + b["price"] * b["quantity"] || 0,
              0
            ),
            details: details,
          })}
          <div className="text-left text-4xl pt-5 py-5">Orders</div>
          <div className="flex" aria-label="Button">
            <button className={buttonClass} onClick={() => this.newItem()}>
              New Item
            </button>
            <button
              className={buttonClass}
              onClick={() => this.submitHandler()}
            >
              Order
            </button>
          </div>
          <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
              <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
                <th scope="col" className="px-6 py-3 w-14 h-14">
                  Image
                </th>
                <th scope="col" className="px-6 py-3 w-14 h-14">
                  Product
                </th>
                <th scope="col" className="px-6 py-3 w-14 h-14">
                  Price
                </th>
                <th scope="col" className="px-6 py-3 w-14 h-14">
                  Quantity
                </th>
                <th scope="col" className="px-6 py-3 w-14 h-14">
                  Amount
                </th>
                <th scope="col" className="px-6 py-3 w-14 h-14">
                  Remove
                </th>
              </tr>
            </thead>
            <tbody>
              {details.map((o: DetailModel, idx) => {
                return (
                  <tr
                    key={idx}
                    className="border-b dark:bg-gray-800 dark:border-gray-700"
                  >
                    <td
                      scope="row"
                      className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                    >
                      <img
                        src={
                          o.product.base64
                            ? o.product.base64
                            : config.noImage
                        }
                      />
                    </td>
                    <td className="px-6 py-4">
                      <input
                        readOnly={true}
                        type="text"
                        className={inputClass}
                        value={o.product.initial}
                      />
                    </td>
                    <td className="px-6 py-4">
                      <input
                        readOnly={true}
                        type="text"
                        className={inputClass}
                        value={o.product.price}
                      />
                    </td>
                    <td className="px-6 py-4">
                      <input
                        type="number"
                        className={inputClass}
                        value={o.quantity}
                        onChange={this.changeHandler(idx)}
                      />
                    </td>
                    <td className="px-6 py-4">
                      <input
                        readOnly={true}
                        type="text"
                        className={inputClass}
                        value={o.product.price * o.quantity}
                      />
                    </td>
                    <td className="px-6 py-4">
                      <button
                        className={buttonClass}
                        onClick={() => this.removeProduct(idx)}
                      >
                        Remove
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
            <tfoot className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
              <th colSpan={3} scope="col" className="px-6 py-3 w-14 h-14">
                Grand Total
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <input
                  readOnly={true}
                  type="number"
                  className={inputClass}
                  value={details.reduce(
                    (a, b) =>
                      parseFloat(a.toString()) +
                        parseFloat(b["quantity"].toString()) || 0,
                    0
                  )}
                />
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <input
                  readOnly={true}
                  type="number"
                  className={inputClass}
                  value={details.reduce(
                    (a, b) =>
                      parseFloat(a.toString()) +
                        parseFloat((b["price"] * b["quantity"]).toString()) ||
                      0,
                    0
                  )}
                />
              </th>
              <th></th>
            </tfoot>
          </table>
          {showModal ? (
            <div className="fixed inset-0 z-50 overflow-hidden">
              <div className="flex justify-center items-center min-h-screen px-4 pt-4 pb-20 text-center">
                <div className="w-full max-w-2xl bg-white dark:bg-gray-900 rounded-lg shadow-lg overflow-hidden overflow-y-auto">
                  <div className="flex items-start justify-between p-5 border-b border-gray-300 dark:border-gray-700 rounded-t">
                    <button
                      className="bg-transparent border-0 text-black float-right"
                      onClick={() => this.setShowModal(false)}
                    >
                      <span className="text-black opacity-7 h-6 w-6 text-xl block bg-gray-400 py-0 rounded-full">
                        x
                      </span>
                    </button>
                  </div>
                  <div className="p-6 overflow-y-auto max-h-96">
                    <ProductList selectProduct={this.selectProduct} />
                  </div>
                  <div
                    className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b"
                    role="group"
                    aria-label="Button group"
                  ></div>
                </div>
              </div>
            </div>
          ) : null}
        </div>
        ;
      </>
    );
  }
}
