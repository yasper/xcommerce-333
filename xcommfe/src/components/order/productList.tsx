import React from "react";
import { IPagination } from "../../interfaces/iPagination";
import { config } from "../../configuration/config";
import { ProductModel } from "../models/productModel";
import { ProductService } from "../../services/productServices";
import { buttonClass, inputClass } from "../style/styleComponent";

interface IProps {
  selectProduct: any;
}
interface IState {
  products: ProductModel[];
  pagination: IPagination;
  imagePreview: any;
}

export default class ProductList extends React.Component<IProps, IState> {
  newPagination: IPagination = {
    pageNum: 1,
    rows: 50,
    search: "",
    orderBy: "",
    sort: 0,
    pages: 0,
  };

  componentDidMount(): void {
    this.loadProduct();
  }

  loadProduct = async () => {
    ProductService.getAll(this.state.pagination)
      .then((result) => {
        if (result.success) {
          this.setState({
            products: result.result,
          });
        }
      })
      .catch((error) => {
        alert(error);
      });
  };
  changeSearch = (name: any) => (event: any) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        [name]: event.target.value,
      },
    });
  };

  constructor(props: IProps) {
    super(props);
    this.state = {
      pagination: this.newPagination,
      products: [],
      imagePreview: config.noImage,
    };
  }

  render() {
    const { products, pagination, imagePreview } = this.state;
    const { selectProduct } = this.props;
    return (
      <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
          <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
            <th colSpan={1} scope="col" className="px-6 py-3 w-14 h-14">
              Search
            </th>
            <th colSpan={2}>
              <input
                className={inputClass}
                type="text"
                required
                value={pagination.search}
                onChange={this.changeSearch("search")}
              />
            </th>
            <th colSpan={1} scope="col" className="px-6 py-3 w-14 h-14">
              <button
                className={buttonClass}
                onClick={() => this.loadProduct()}
              >
                Filter
              </button>
            </th>
          </tr>
          <tr>
            <th></th>
            <th scope="col" className="px-6 py-3 w-14 h-14">
              Initial/Name
            </th>
            <th scope="col" className="px-6 py-3 w-14 h-14">
              Price
            </th>
            <th scope="col" className="px-6 py-3 w-14 h-14">
              Stock
            </th>
          </tr>
        </thead>
        <tbody>
          {products.map((o) => {
            return (
              <tr
                className="border-b dark:bg-gray-800 dark:border-gray-700"
                onClick={() => selectProduct(o.id)}
              >
                <td className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                  <img
                    height="128"
                    width="128"
                    src={o.base64 ? o.base64 : imagePreview}
                  />
                </td>
                <td className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                  {o.initial}/{o.name}
                </td>
                <td className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                  {o.price}
                </td>
                <td className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                  {o.stock}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}
