import React from "react";
import { IGallery } from "../../interfaces/iGallery";
import { config } from "../../configuration/config";
import { iProduct } from "../../interfaces/iProduct";
import { GalleryService } from "../../services/galleryService";
import { IPagination } from "../../interfaces/iPagination";

interface IProps {
  //prop memanagement data dari parent atau luar
  selectedImage: any;
}

interface IState {
  //state itu memanagement data didalam
  imagePreview: any;
  pagination: IPagination;
  galleries: IGallery[];
}

export default class FormGallery extends React.Component<IProps, IState> {
  newPagination: IPagination = {
    pageNum: 1,
    rows: config.rowPerPage[0],
    search: "",
    orderBy: "",
    sort: 0,
    pages: 0,
  };
  constructor(props: IProps) {
    super(props);
    this.state = {
      imagePreview: config.noImage,
      pagination: this.newPagination,
      galleries: [],
    };
  }

  componentDidMount(): void {
    this.loadGallery();
  }
  loadGallery = async () => {
    const { pagination } = this.state;
    const galResult = await GalleryService.getAll(pagination);
    if (galResult.success) {
      this.setState({
        galleries: galResult.result,
        pagination: {
          ...this.state.pagination,
          pages: galResult.pages,
        },
      });
    }
  };
  handleImageClick = (selectedImage: number) => {
    this.props.selectedImage(selectedImage);
  };
  changePage = (direction: any) => {
    const { pagination } = this.state;
    let newPageNum = pagination.pageNum;

    if (direction === "prev") {
      newPageNum -= 1;
      if (newPageNum < 1) {
        newPageNum = pagination.pages;
      }
    } else if (direction === "next") {
      newPageNum += 1;
      if (newPageNum > pagination.pages) {
        newPageNum = 1;
      }
    }

    this.setState(
      {
        pagination: {
          ...pagination,
          pageNum: newPageNum,
        },
      },
      () => {
        this.loadGallery();
      }
    );
  };

  render() {
    const { galleries, pagination } = this.state;
    return (
      <div>
        <div className="m-6 grid grid-cols-2 md:grid-cols-3 gap-4 overflow-y-auto max-h-96">
          {galleries.map((o: IGallery) => {
            return (
              <div>
                <div>
                  <img
                    className="m-3 max-w-full h-auto rounded-lg"
                    src={o.base64Big}
                    alt=""
                    onClick={() => this.handleImageClick(o.id)}
                  />
                </div>
              </div>
            );
          })}
        </div>
        <div className="mb-6">
          <button
            className={`mx-2 px-2 py-1 border text-slate-500 rounded-md focus:outline-none focus:ring focus:border-blue-300 ${
              pagination.pages <= 1 ? "hidden" : "visible"
            }`}
            onClick={() => this.changePage("prev")}
          >
            Previous
          </button>
          <span className="text-slate-800">{pagination.pageNum}</span>
          <button
            className={`mx-2 px-2 py-1 border text-slate-500 rounded-md focus:outline-none focus:ring focus:border-blue-300 ${
              pagination.pages !== 1 ? "visible" : "hidden"
            }`}
            onClick={() => this.changePage("next")}
          >
            Next
          </button>
        </div>
      </div>
    );
  }
}
