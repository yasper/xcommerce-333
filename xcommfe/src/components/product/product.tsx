import React from "react";
import { iProduct } from "../../interfaces/iProduct";
import { ProductService } from "../../services/productServices";
import Form from "./form";
import { iVariant } from "../../interfaces/iVariant";
import { ECommand } from "../../enums/eCommand";
import { VariantService } from "../../services/variantService";
import { error } from "console";
import { IPagination } from "../../interfaces/iPagination";
import { config } from "../../configuration/config";
import { iCategory } from "../../interfaces/iCategory";
import { CategoryService } from "../../services/categoryServices";
import { Category } from "../category";
import { IGallery } from "../../interfaces/iGallery";
import { GalleryService } from "../../services/galleryService";
import FormGallery from "./formGallery";

interface IProps {}

interface IState {
  categories: iCategory[];
  products: iProduct[];
  galleries: IGallery[];
  variants: iVariant[];
  product: iProduct;
  command: ECommand;
  showModal: boolean;
  showAllImage: boolean;
  pagination: IPagination;
  gallery: IGallery;
}

export default class Product extends React.Component<IProps, IState> {
  newPagination: IPagination = {
    pageNum: 1,
    rows: config.rowPerPage[0],
    search: "",
    orderBy: "",
    sort: 0,
    pages: 0,
  };
  newCategory: iCategory = {
    id: 0,
    initial: "",
    name: "",
    active: false,
  };
  newVariant: iVariant = {
    id: 0,
    category: this.newCategory,
    categoryId: 0,
    initial: "",
    name: "",
    active: false,
  };
  newGallery: IGallery = {
    id: 0,
    title: "",
    base64Big: "",
    base64Small: "",
    active: false,
  };
  newProduct: iProduct = {
    id: 0,
    variantId: 0,
    initial: "",
    name: "",
    base64: "",
    categoryId: 0,
    galleryId: 0,
    variant: this.newVariant,
    description: "",
    price: 0,
    stock: 0,
    active: false,
  };
  constructor(props: IProps) {
    super(props);
    this.state = {
      categories: [],
      products: [],
      variants: [],
      pagination: this.newPagination,
      product: this.newProduct,
      showModal: false,
      showAllImage: false,
      command: ECommand.create,
      galleries: [],
      gallery: this.newGallery,
    };
  }

  componentDidMount(): void {
    this.loadProduct();
  }

  loadProduct = async () => {
    const { pagination } = this.state;
    const result = await ProductService.getAll(pagination);
    if (result.success) {
      const catResult = await CategoryService.getAllNoPag();
      if (catResult.success) {
        this.setState({
          categories: catResult.result,
        });
      }
      this.setState({
        products: result.result,
        pagination: {
          ...this.state.pagination,
          pages: result.pages,
        },
      });
    } else {
      alert("Error: " + result.result);
    }
  };

  setShowModal = (val: boolean) => {
    this.setState({
      showModal: val,
    });
    console.log(this.state.showModal);
  };
  setShowAllImage = (val: boolean) => {
    this.setState({
      showAllImage: val,
    });
  };
  changeHandler = (name: any) => (event: any) => {
    console.log(name);
    if (name === "categoryId") {
      VariantService.getByParentId(event.target.value).then((result) => {
        this.setState({
          variants: result.result,
        });
      });
    }
    this.setState({
      product: {
        ...this.state.product,
        [name]: event.target.value,
      },
    });
  };

  updatePicture = async (id: number) => {
    await ProductService.getById(id)
      .then((result) => {
        if (result.success) {
          this.setState({
            showAllImage: true,
            product: result.result,
            command: ECommand.addImage,
          });
        } else {
          alert("Error result " + result.result);
        }
      })
      .catch((error) => {
        alert("Error error" + error);
      });
  };

  selectGalery = async (galleryId: number) => {
    const { id } = this.state.product;
    ProductService.changeImage(id, galleryId)
      .then((result) => {
        if (result.success) {
          this.setState({
            showAllImage: false,
          });
          this.loadProduct();
        } else {
          alert("Error result " + result.result);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  checkBoxHandler = (name: any) => (event: any) => {
    this.setState({
      product: {
        ...this.state.product,
        [name]: event.target.checked,
      },
    });
  };

  creatCommand = () => {
    this.setState({
      command: ECommand.create,
      product: this.newProduct,
      showModal: true,
    });
    // this.setShowModal(true)
  };

  updateCommand = async (id: number) => {
    await ProductService.getById(id)
      .then((result) => {
        if (result.success) {
          this.setState({
            showModal: true,
            command: ECommand.edit,
            product: result.result,
          });
          this.loadProduct();
        } else {
          alert("Error result: " + result.result);
        }
      })
      .catch((error) => {
        alert("Error error: " + error);
      });
  };

  changeStatusCommand = async (id: number) => {
    await ProductService.getById(id)
      .then((result) => {
        if (result.success) {
          this.setState({
            showModal: true,
            command: ECommand.changeStatus,
            product: result.result,
          });
          this.loadProduct();
        } else {
          alert("Error result: " + result.result);
        }
      })
      .catch((error) => {
        alert("Error error: " + error);
      });
  };

  changeRowPerPage = (name: any) => (event: any) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        [name]: event.target.value,
      },
    });
    new Promise(() => {
      setTimeout(() => {
        this.loadProduct();
      }, 500);
    });
  };

  changeSearch = (name: any) => (event: any) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        [name]: event.target.value,
      },
    });
  };

  changeOrder = (order: string) => {
    this.setState({
      pagination: {
        ...this.state.pagination,
        orderBy: order,
      },
    });
    new Promise(() => {
      setTimeout(() => {
        this.loadProduct();
      }, 500);
    });
  };

  submitHandler = async () => {
    const { command } = this.state;
    if (command === ECommand.create) {
      await ProductService.post(this.state.product)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              product: this.newProduct,
            });
            this.loadProduct();
          } else {
            alert("Error result: " + result.result);
          }
        })
        .catch((error) => {
          alert("Error error: " + error);
        });
    } else if (command === ECommand.edit) {
      await ProductService.update(this.state.product.id, this.state.product)
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              product: this.newProduct,
            });
            this.loadProduct();
          } else {
            alert("Error result: " + result.result);
          }
        })
        .catch((error) => {
          alert("Error error: " + error);
        });
    } else if (command === ECommand.changeStatus) {
      await ProductService.changeStatus(
        this.state.product.id,
        this.state.product.active
      )
        .then((result) => {
          if (result.success) {
            this.setState({
              showModal: false,
              product: this.newProduct,
            });
            this.loadProduct();
          } else {
            alert("Error result: " + result.result);
          }
        })
        .catch((error) => {
          alert("Error error: " + error);
        });
    }
  };
  render() {
    const {
      categories,
      variants,
      products,
      product,
      showModal,
      showAllImage,
      command,
      pagination,
      gallery,
      galleries,
    } = this.state;
    const loopPages = () => {
      let content: any = [];
      for (let page = 1; page <= pagination.pages; page++) {
        content.push(<option value={page}>{page}</option>);
      }
      return content;
    };
    return (
      <div>
        <div className="text-left text-4xl pt-5 py-5">Product</div>
        <span>{JSON.stringify(product)}</span>
        <div className="flex" aria-label="Button">
          <button
            className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded focus:shadow-outline hover:bg-green-800"
            onClick={() => this.creatCommand()}
          >
            Create New
          </button>
        </div>
        <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
              <th colSpan={1} scope="col" className="px-6 py-3 w-14 h-14">
                {/* grow flex-none  */}
                Search
              </th>
              <th colSpan={4} scope="col" className="px-6 py-3 w-14 h-14">
                <input
                  type="text"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  required
                  value={pagination.search}
                  onChange={this.changeSearch("search")}
                />
              </th>
              <th colSpan={1} scope="col" className="px-6 py-3 w-14 h-14">
                <button
                  className="h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg rounded-r-lg focus:shadow-outline hover:bg-green-800"
                  onClick={() => this.loadProduct()}
                >
                  Filter
                </button>
              </th>
              <th colSpan={3} scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowPerPage("sort")}
                >
                  <option value="0">Asc</option>
                  <option value="1">Desc</option>
                </select>
              </th>
            </tr>
            <tr className="border-b dark:bg-gray-900 dark:border-gray-700">
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14"
                onClick={() => this.changeOrder("")}
              >
                {/* grow flex-none  */}
                Image
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14"
                onClick={() => this.changeOrder("initial")}
              >
                Initial
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14"
                onClick={() => this.changeOrder("name")}
              >
                Name
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Variant/Category Initial
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Description
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14"
                onClick={() => this.changeOrder("price")}
              >
                Price
              </th>
              <th
                scope="col"
                className="px-6 py-3 w-14 h-14"
                onClick={() => this.changeOrder("stock")}
              >
                Stock
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Active
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {products?.map((cat: iProduct) => {
              return (
                <tr
                  key={cat.id}
                  className="border-b dark:bg-gray-800 dark:border-gray-700"
                >
                  <td
                    scope="row"
                    className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                    onClick={() => this.updatePicture(cat.id)}
                  >
                    <img
                      width="128"
                      height="128"
                      src={
                        cat.base64
                          ? cat.base64
                          : config.noImage
                      }
                    ></img>
                  </td>
                  <td className="px-6 py-4">{cat.initial}</td>
                  <td className="px-6 py-4">{cat.name}</td>
                  <td className="px-6 py-4">
                    {cat.variant.name}/{cat.variant.category.name}
                  </td>
                  <td className="px-6 py-4">{cat.description}</td>
                  <td className="px-6 py-4">{cat.price}</td>
                  <td className="px-6 py-4">{cat.stock}</td>
                  <td className="px-6 py-4">
                    <div className="flex items-center">
                      <input
                        id="checked-checkbox"
                        type="checkbox"
                        checked={cat.active}
                        className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                      />
                    </div>
                  </td>
                  <td className="px-6 py-4">
                    <div
                      className="inline-flex"
                      role="group"
                      aria-label="Button group"
                    >
                      <button
                        className="h-10 px-5 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg focus:shadow-outline hover:bg-green-800"
                        onClick={() => this.updateCommand(cat.id)}
                      >
                        Edit
                      </button>
                      <button
                        className="h-10 px-5 text-blue-100 transition-colors duration-150 bg-blue-700 rounded-r-lg focus:shadow-outline hover:bg-blue-800"
                        onClick={() => this.changeStatusCommand(cat.id)}
                      >
                        Status
                      </button>
                    </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
          <tfoot className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                <label className="block text-sm font-medium text-gray-900 dark:text-white">
                  Rows per page
                </label>
              </th>
              <th colSpan={2} scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowPerPage("rows")}
                >
                  {config.rowPerPage.map((o: number) => {
                    return <option value={o}>{o}</option>;
                  })}
                </select>
              </th>
              <th scope="col" className="px-6 py-3 w-14 h-14"></th>
              <th scope="col" className="px-6 py-3 w-14 h-14"></th>
              <th scope="col" className="px-6 py-3 w-14 h-14">
                Page:
              </th>
              <th colSpan={3} scope="col" className="px-6 py-3 w-14 h-14">
                <select
                  id="countries"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={this.changeRowPerPage("pageNum")}
                >
                  {loopPages()}
                </select>
              </th>
            </tr>
          </tfoot>
        </table>
        {showModal ? (
          <div className="fixed inset-0 z-50 overflow-hidden">
            <div className="flex justify-center items-center min-h-screen px-4 pt-4 pb-20 text-center">
              <div className="w-full max-w-md bg-white dark:bg-gray-900 rounded-lg shadow-lg overflow-hidden overflow-y-auto">
                <div className="flex items-start justify-between p-5 border-b border-gray-300 dark:border-gray-700 rounded-t">
                  <h3 className="text-3xl text-gray-900 dark:text-white">
                    {command.valueOf()}
                  </h3>
                  <button
                    className="bg-transparent border-0 text-black float-right"
                    onClick={() => this.setShowModal(false)}
                  >
                    <span className="text-black opacity-7 h-6 w-6 text-xl block bg-gray-400 py-0 rounded-full">
                      x
                    </span>
                  </button>
                </div>
                <div className="p-6 overflow-y-auto max-h-96">
                  <Form
                    categories={categories}
                    command={command}
                    variants={variants}
                    product={product}
                    changeHandler={this.changeHandler}
                    checkBoxHandler={this.checkBoxHandler}
                    galleries={galleries}
                  />
                </div>
                <div
                  className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b"
                  role="group"
                  aria-label="Button group"
                >
                  <button
                    className="my-8 justify-start h-8 px-4 text-green-100 transition-colors duration-150 bg-green-700 rounded-l-lg focus:shadow-outline hover:bg-green-800"
                    onClick={() => this.setShowModal(false)}
                  >
                    Close
                  </button>
                  <button
                    className="my-8 justify-start h-8 px-4 text-blue-100 transition-colors duration-150 bg-blue-700 rounded-r-lg focus:shadow-outline hover:bg-blue-800"
                    onClick={() => this.submitHandler()}
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : null}
        {showAllImage ? (
          <>
            <div className="fixed inset-0 z-50 overflow-hidden">
              <div className="flex justify-center items-center min-h-screen px-4 pt-4 pb-20 text-center">
                <div className="w-full max-w-md bg-white dark:bg-gray-900 rounded-lg shadow-lg overflow-hidden overflow-y-auto">
                  <div className="flex items-start justify-between p-5 border-b border-gray-300 dark:border-gray-700 rounded-t">
                    <h3 className="text-3xl text-gray-900 dark:text-white">
                      {command.valueOf()}
                    </h3>
                    <button
                      className="bg-transparent border-0 text-black float-right"
                      onClick={() => this.setShowAllImage(false)}
                    >
                      <span className="text-black opacity-7 h-6 w-6 text-xl block bg-gray-400 py-0 rounded-full">
                        x
                      </span>
                    </button>
                  </div>
                  <div>
                    <FormGallery selectedImage={this.selectGalery} />
                  </div>
                </div>
              </div>
            </div>
          </>
        ) : null}
      </div>
    );
  }
}
