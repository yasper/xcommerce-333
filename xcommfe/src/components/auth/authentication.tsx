import React from "react";
import { IAuthentication } from "../../interfaces/iAuth";
import {
  buttonClass,
  buttonClassLogout,
  inputClass,
  labelClass,
} from "../style/styleComponent";
import { AuthService } from "../../services/authService";
import { Navigate } from "react-router-dom";
import { withRouter } from "../layout/withRouter";
interface IProps {
  logged: boolean;
  changeLoggedHandler: any;
  navigate: any;
}

interface IState {
  auth: IAuthentication;
  shouldRedirect: boolean;
}

class Authentication extends React.Component<IProps, IState> {
  newAuth: IAuthentication = { userName: "", password: "" };
  constructor(props: IProps) {
    super(props);
    this.state = {
      auth: this.newAuth,
      shouldRedirect: false,
    };
    this.props.changeLoggedHandler(false);
  }

  componentDidMount(): void {
    {
      AuthService.logout();
    }
  }
  logout = () => {
    AuthService.logout();
    alert("logout success");
    this.setState({ shouldRedirect: true }, () => {
      // Reload the page after a short delay
      setTimeout(() => {
        window.location.reload();
      }, 1000); // Adjust the delay as needed
    });
  };

  changeHandler = (name: any) => (event: any) => {
    this.setState({
      auth: {
        ...this.state.auth,
        [name]: event.target.value,
      },
    });
  };

  handleSubmit = () => {
    const { auth } = this.state;
    const { changeLoggedHandler } = this.props;

    AuthService.login(auth)
      .then((result) => {
        // console.log(result.result);
        if (result.success) {
          changeLoggedHandler(true);
          this.props.navigate("/");
        } else {
          alert("Fail Login");
        }
      })
      .catch((error) => {
        alert("error: " + error);
      });
  };

  render() {
    const { auth } = this.state;
    return (
      <div className="fixed inset-0 z-50 overflow-hidden">
        <div className="flex justify-center items-center min-h-screen px-4 pt-4 pb-20 text-center">
          <div className="w-full max-w-md bg-white dark:bg-gray-900 rounded-lg shadow-lg overflow-hidden overflow-y-auto">
            <div className="flex items-start justify-between p-5 border-b border-gray-300 dark:border-gray-700 rounded-t">
              <h3 className="text-3xl text-gray-900 dark:text-white">Login</h3>
            </div>
            <div className="relative p-6 flex-auto">
              <div className="mb-6">
                <label className={labelClass}>Username</label>
                <input
                  type="text"
                  id="userName"
                  className={inputClass}
                  required
                  value={auth.userName}
                  onChange={this.changeHandler("userName")}
                />
              </div>
              <div className="mb-6">
                <label className={labelClass}>Password</label>
                <input
                  type="password"
                  id="password"
                  className={inputClass}
                  required
                  value={auth.password}
                  onChange={this.changeHandler("password")}
                />
              </div>
              <div className="flex justify-center" aria-label="Button">
                <button className={buttonClass} onClick={this.handleSubmit}>
                  Login
                </button>
                <button className={buttonClassLogout} onClick={this.logout}>
                  Logout
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Authentication);
