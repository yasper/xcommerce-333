export class GalleryModel {
  id: number = 0;
  title: string = "";
  base64Big: any;
  base64Small: any;
  active: boolean = false;
}
