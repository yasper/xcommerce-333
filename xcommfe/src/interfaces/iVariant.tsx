import { iCategory } from "./iCategory";

export interface iVariant extends iCategory {
    categoryId: number;
    category: iCategory;
}