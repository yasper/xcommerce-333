export interface iCategory {
    id: number;
    initial: string;
    name: string;
    active: boolean;
}