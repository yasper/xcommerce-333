import { iCategory } from "./iCategory";
import { IGallery } from "./iGallery";
import { iVariant } from "./iVariant";

export interface iProduct extends iCategory {
  variantId: number;
  categoryId: number;
  variant: iVariant;
  base64: string;
  description: string;
  price: number;
  stock: number;
  galleryId: number;
}
